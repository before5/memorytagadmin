package com.before5.persistence;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.before5.domain.MemberVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class JoinTest {

	@Inject
	public JoinDAO dao;
	
	@Test
	public void test() throws Exception{
		MemberVO vo = new MemberVO();
		vo.setId("ji");
		vo.setPw("1234");
		vo.setPhone("01033454395");
		vo.setGender("W");
		vo.setBirth("19920506");
		vo.setGrade(3);
		vo.setName("jisun");
		vo.setEmail("4484395@naver.com");
		dao.joinMember(vo);
	}
	@Test
	public void testId() throws Exception{
		System.out.println(dao.searchId("kjs111111"));
		
	}

}
