package com.before5.persistence;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.before5.domain.NodeVO;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class NodeTest {

	@Inject
	public NodeDAO dao;
	@Test
	public void test() {
		List<NodeVO> vo = dao.select();
		for (NodeVO nodeVO : vo) {
			System.out.println(nodeVO);
		}
		
	}

}
