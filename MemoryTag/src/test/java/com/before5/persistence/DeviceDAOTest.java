package com.before5.persistence;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.before5.controller.NoticeController;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class DeviceDAOTest {

	private static final Logger logger = LoggerFactory.getLogger(DeviceDAOTest.class);
	
	@Inject
	private DeviceDAO dao;
	
	@Test
	public void testlistDevice() throws Exception {
	  System.out.println(dao.list());
	}

}
