package com.before5.persistence;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.before5.controller.NoticeController;
import com.before5.domain.Criteria;
import com.before5.domain.NoticeFileVO;
import com.before5.domain.NoticeVO;
import com.before5.domain.SearchCriteria;
import com.before5.util.UploadFileUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class NoticeDAOTest {

	private static final Logger logger = LoggerFactory.getLogger(NoticeDAOTest.class);
	@Inject
	private NoticeDAO dao;

	@Test
	public void testCreate() throws Exception {
		NoticeVO vo = new NoticeVO();
		vo.setTitle("�׽�Ʈ1");
		vo.setContent("�׽�Ʈ����");
		vo.setId("test");
		System.out.println(vo);
		dao.create(vo);
	}

	@Test
	public void testRead() throws Exception {

		System.out.println(dao.read(2));
	}

	@Test
	public void testUpdate() throws Exception {
		NoticeVO vo = new NoticeVO();
		vo.setTitle("�׽�Ʈ2");
		vo.setContent("�׽�Ʈ����2");
		vo.setNno(2);
		System.out.println(vo);
		dao.update(vo);
	}

	@Test
	public void testDelete() throws Exception {

		dao.delete(2);
	}

//	@Test
//	public void testlistAll() throws Exception {
//
//		List<NoticeVO> list = dao.listAll();
//		for (int i=0; i<list.size(); i++) {
//			System.out.println(list.get(i));
//		}
//		
//	}
	
//	@Test
//	public void testlistPage() throws Exception {
//
//		int page=2;
//		List<NoticeVO> list = dao.listPage(page);
//		for (int i=0; i<list.size(); i++) {
//			System.out.println(list.get(i));
//		}
//		
//	}
	
	@Test
	public void testCriteria() throws Exception {

		Criteria cri = new Criteria();
		cri.setPage(2);
		cri.setPerPageNum(5);
		
		List<NoticeVO> list = dao.listCriteria(cri);
		for (int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
		
	}
	
	@Test
	public void testURI() throws Exception {

		UriComponents uriComponents = UriComponentsBuilder.newInstance().path("/{module}/{page}").queryParam("nno", "12")
				.queryParam("perPageNum", 20).build().expand("notice","read").encode();
		
		logger.info("/notice/read?nno=12&perPageNum=20");
		logger.info(uriComponents.toString());
	}
	

	@Test
	public void testDynamic1() throws Exception {

		SearchCriteria cri = new SearchCriteria();
		cri.setPage(2);
		cri.setKeyword("yt");
		cri.setSearchType("t");
		
		logger.info("=============================");
		
		List<NoticeVO> list = dao.listSearch(cri);
		
		for (NoticeVO noticeVO : list) {
			logger.info(noticeVO.getNno() +": "+ noticeVO.getTitle());
		}
		
		logger.info("=============================");
		
		logger.info("COUNT: " + dao.listSearchCount(cri));
		}
	
	
	@Test
	public void testFileUpload() throws Exception {

		NoticeFileVO filevo = new NoticeFileVO();
		filevo.setOriginal_file_name("복실이");
		filevo.setExtension("jpeg");
		filevo.setFiledirectory("C:\\memorytag\\notice\\2016\\05\\04");
		filevo.setStored_file_name("abc_복실이");
		filevo.setFile_size(1234);
		System.out.println(filevo);
		
		
		
		
		
		
		
		
		dao.addAttach(filevo);
		}
	
}
