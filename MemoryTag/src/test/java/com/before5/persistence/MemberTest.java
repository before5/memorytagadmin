package com.before5.persistence;

import java.util.List;


import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.before5.domain.Criteria;
import com.before5.domain.MemberVO;
import com.before5.domain.SearchCriteria;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class MemberTest {

	private static final Logger logger = LoggerFactory.getLogger(MemberTest.class);
	
	
	@Inject
	private MemberDAO dao;
	@Test
	public void testListPage()throws Exception{
		
		int page=2;
		
		List<MemberVO> list = dao.listPage(page);
	
		System.out.println(list);
		
		for (MemberVO memberVO : list) {
		
			logger.info(memberVO.getId()+":"+memberVO.getPw());
		}
	}
	@Test
	public void testListCriteria()throws Exception{
		
		Criteria cri = new Criteria();
		cri.setPage(1);
		cri.setPerPageNum(20);
		
		List<MemberVO> list = dao.listCriteria(cri);
		
		for (MemberVO memberVO : list) {
			logger.info(memberVO.getId()+":"+memberVO.getName());
		}
		}
	
	@Test
	public void countPagingtest() throws Exception{
		Criteria cri = new Criteria();
		System.out.println(dao.countPaging(cri));
		
	}
	@Test
	public void URITest()throws Exception{				//UriComponents의 기능을 확인해보는 Test
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.path("/admin/read")
				.queryParam("rownum",12)
				.queryParam("perPageNum", 20)
				.build();
		
		logger.info("/admin/read?rownum=12&perPageNum=20");
		logger.info(uriComponents.toString());
	}
	@Test
	public void URITest2()throws Exception{				
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.path("/{module}/{page}")
				.queryParam("rownum",12)
				.queryParam("perPageNum", 20)
				.build()
				.expand("admin","read")
				.encode();
		
		logger.info("/admin/read?rownum=12&perPageNum=20");
		logger.info(uriComponents.toString());
	}

	
	@Test
	public void testDynamic1()throws Exception{
		SearchCriteria cri = new SearchCriteria();
		cri.setPage(3);
		cri.setKeyword("sang");
		cri.setSearchType("i");
		
		logger.info("검색 시작");
		List<MemberVO> list =dao.listSearch(cri);
		
		for (MemberVO memberVO : list) {
			logger.info(memberVO.getId()+":"+memberVO.getName());			
			
		}
		logger.info("숫자 카운트");
		logger.info("총 숫자는"+dao.listSearchCount(cri));
	}
	

	@Test
	public void removeTest(){
		dao.deleteMember("sang26");
	}
	
	@Test
	public void editTest()throws Exception{
		
		MemberVO vo = new MemberVO();
		
		vo.setId("young16192");
		vo.setGrade(1);
		
		dao.updateMember(vo);
		}
	
}
