package com.before5.persistence;

import static org.junit.Assert.fail;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.before5.domain.ReservationVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class ApplicationTest {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationTest.class);
	
	@Inject
	ReservationDAO dao;
	
	@Test
	public void test() {
		fail("Not yet implemented");
	}
	
	@Test
	public void registeContentTest() throws Exception{
		ReservationVO vo = new ReservationVO();
		vo.setTitle("contentTest");
		vo.setOrderdate("2016050611");
		dao.registerContent(vo);
	}
	@Test
	public void registerFile() throws Exception{
		ReservationVO vo = new ReservationVO();
		vo.setFileName("fileTest");
		vo.setType(".jpg");
		dao.registerFile(vo);
	}
	
	@Test
	public void registerOrder() throws Exception{
		String id = "kjs";
		int[] dnos = {205,206};
		String pw="1234";
		for (int dno : dnos) {
			dao.registerOrder(id, dno,pw);
		}
		
		
	}

}
