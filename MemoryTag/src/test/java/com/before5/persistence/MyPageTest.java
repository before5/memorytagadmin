package com.before5.persistence;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.before5.domain.Criteria;
import com.before5.domain.MyPageVO;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class MyPageTest {

	@Inject 
	MyPageDAO dao;
	
	@Test
	public void test() throws Exception{
		List<MyPageVO> list = dao.myOrder("kjs4395");
		for (MyPageVO vo : list) {
			System.out.println(vo);
		}
	}
	
	@Test
	public void listTest() throws Exception{
		Criteria cri = new Criteria();
		List<MyPageVO> list = dao.myListOrder("kjs", cri);
		for (MyPageVO vo : list) {
			System.out.println(vo);
		}
	}

	@Test
	public void countTest() throws Exception{
		System.out.println(dao.countOrder("kjs"));
	}
	
	@Test
	public void readTest() throws Exception{
		System.out.println(dao.readOrder("kjs", 213));
	}
	
	@Test
	public void updateTest() throws Exception{
		MyPageVO vo = new MyPageVO();
		vo.setCno(758);
		vo.setTitle("dao는 제");
		vo.setText("제대로 왜");
		vo.setPw("1234");
		System.out.println(vo);
		dao.updateContent(vo);
		System.out.println(vo);
	}
}
