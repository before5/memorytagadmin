package com.before5.persistence;


import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.before5.domain.LoginVO;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class LoginTest {		//Login 관련 테스트

	@Inject
	private LoginDAOImpl dao;				//DAO를 주입해야한다. DB랑 데이터교환은 DAO를 통하기 떄문에
	
	@Test
	public void daotest() throws Exception {	//DAO에서 DB랑 데이터가 잘 이루어지는지 체크합니다.
		LoginVO vo = new LoginVO();			//vo객체를 만듭니다. vo에 데이터를 넣기 위해서 입니다.
		
	
		vo.setId("kjs1");		//vo Id에 "kjs"를 넣는다.
		vo.setPw("kjs");		//pw pw에 "kjs"를 넣는다.
		dao.login(vo);			//vo의 데이터를 가지고 로그인 처리 즉select id, pw from tbl_member where id='kjs' and pw='kjs'; 이걸 날린다.
		
		System.out.println(vo.toString());// vo에 데이터가 잘들어 갔는디 확인해 보았다.
		System.out.println("로그인댐"+dao.login(vo));	//쿼리문이 실행되면 맞는 결과 값이 돌아올것이다. 그럼 이 syso로 확인이 가능하다.
		
	}
}
