package com.before5.service;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.before5.controller.ApplicationTest;
import com.before5.domain.MemberVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class MemberServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ApplicationTest.class);

	@Inject
	MemberServiceImpl service;
	
	@Test
	public void test() throws Exception{
		List<MemberVO> list=service.readAllMember();
		for (MemberVO memberVO : list) {
			System.out.println(memberVO);
		}
	}

}
