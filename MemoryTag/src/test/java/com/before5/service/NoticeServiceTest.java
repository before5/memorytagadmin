package com.before5.service;

import static org.junit.Assert.*;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.before5.controller.ApplicationTest;
import com.before5.domain.NoticeVO;
import com.before5.domain.SearchCriteria;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class NoticeServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(ApplicationTest.class);

	@Inject
	NoticeServiceImpl service;
	
	@Test
	public void testDynamic1() throws Exception {

		SearchCriteria cri = new SearchCriteria();
		cri.setPage(1);
		cri.setKeyword("yt");
		cri.setSearchType("t");
		
		logger.info("=============================");
		
		List<NoticeVO> list = service.listSearchCriteria(cri);
		
		for (NoticeVO noticeVO : list) {
			logger.info(noticeVO.getNno() +": "+ noticeVO.getTitle());
		}
		
		logger.info("=============================");
		
		logger.info("COUNT: " + service.listSearchCount(cri));
		}

}
