package com.before5.service;

import static org.junit.Assert.fail;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.before5.domain.ReservationVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class ApplicationServiceTest {

	@Inject 
	ReservationService service;
	
	@Test
	public void test() {
		fail("Not yet implemented");
	}
	
	@Test
	public void registContent() throws Exception{
		ReservationVO vo = new ReservationVO();
		vo.setTitle("contentTest2222");
		vo.setOrderdate("2016050711");
		service.registerContent(vo);
	}
	
	@Test
	public void registerFile() throws Exception{
		ReservationVO vo = new ReservationVO();
		vo.setFileName("fileTest2222");
		vo.setType(".jpg");
		service.registerFile(vo);
	}
	
	@Test
	public void registerOrder() throws Exception{
		String id = "kjs";
		int[] dnos = {205,206};
		String pw = "1234";
		for (int dno : dnos) {
			service.registerOrder(id, dno,pw);
		}
	}

}
