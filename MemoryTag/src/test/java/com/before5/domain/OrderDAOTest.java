package com.before5.domain;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.before5.persistence.OrderDAO;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class OrderDAOTest {
	
	@Inject
	private OrderDAO dao;

	@Test
	public void testDynamic1() throws Exception {
		
		SearchCriteria cri = new SearchCriteria();
		cri.setPage(1);
		cri.setKeyword("11");
		cri.setSearchType("c");
		
		List<OrderVO> list = dao.listSearch(cri);
		
		for (OrderVO orderVO : list) {
			System.out.println(orderVO.getOno()+ ":" + orderVO.getOrderDate() 
			+ ":" + orderVO.getId());
		}
		
		System.out.println("content:" + dao.listSearchCount(cri));
		
			
		}
		
		
	}


