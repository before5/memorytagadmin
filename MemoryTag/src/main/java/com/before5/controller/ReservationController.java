package com.before5.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.before5.domain.DeviceVO;
import com.before5.domain.LoginVO;
import com.before5.domain.ReservationVO;
import com.before5.service.DeviceService;
import com.before5.service.ReservationService;

import com.before5.util.MediaUtils;

import com.before5.util.UploadFileUtils;

@Controller
@RequestMapping("/reservation/*")
public class ReservationController {

	private static final Logger logger = LoggerFactory.getLogger(ReservationController.class);

	@Inject
	ReservationService service;

	@Inject
	DeviceService dservice;

	@RequestMapping(value = "/reserve", method = RequestMethod.GET)
	public void applyGet(ReservationVO vo, Model model, HttpSession session) {
		Object user = session.getAttribute("login");
		LoginVO login = (LoginVO) user;
		String id = login.getId(); // 로그인 한 아이디를 가지고 온다.
		System.out.println(id);
		vo.setId(id); // id를 셋팅해준다!!
		logger.info("예약등록페이지에 들어왔다.");
		// 일단 jsp파일만 보여지게 만든다아.
	}

	@RequestMapping(value = "/reserve", method = RequestMethod.POST)
	public String applyPost(@ModelAttribute("vo") ReservationVO vo, @RequestParam("file") MultipartFile[] files) throws Exception {

		logger.info("예약 post 들어왔다");
		System.out.println(vo);

		Calendar cal = Calendar.getInstance(); // 날짜를 가져온다.
		String year = File.separator + cal.get(Calendar.YEAR); // 구분자+년도=1
		
		String month = year + File.separator + new DecimalFormat("00").format(cal.get(Calendar.MONTH) + 1); // 년도
																											// +
																											// 구분자+월
		String date = month + File.separator + new DecimalFormat("00").format(cal.get(Calendar.DATE)); //
		// 년도 + 구분자+월+일
		logger.info(date + File.separator);
		String makeDir = "C:\\zzz\\" + date + File.separator; //

		vo.setDirectory("C:\\zzz\\" + date);
		File dir = new File(makeDir);
		if (dir.exists() == false) {
			dir.mkdirs();
		} // 폴더가 없으면 만들고
		
		for (MultipartFile file : files) {
			UUID uid = UUID.randomUUID();
			String savedName = uid.toString() + "_" + file.getOriginalFilename(); //savedname = uuid+_+originalname

			Integer size = (int) file.getSize();
			vo.setSize(size);
			File target = new File(makeDir + savedName); // 요기에 파일을 만들어야 한다.
			
			//public static String makeThumbnail(String uploadPath, String path, String fileName)

			
			try {
				
				FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(target));
				vo.setFileName(savedName); // vo의 fileName을 savedName으로 저장해준다.
				String[] name = savedName.split("\\."); // 정규 표현식 때문에 .은 //.으로
														// 인식해야 한다.
				String type = name[1];// 확장자
				System.out.println(type);
				String contentType = file.getContentType();
				

				if(MediaUtils.getMediaType(type) != null){
					
					String thumnail = UploadFileUtils.makeThumbnail("C:\\zzz\\", date, savedName);
					// 썸네일 만들어야 한다.
					System.out.println("thumnail"+thumnail+"썸네일 만들었다."); // 썸네일 생성완료!!

				}
				

				vo.setType(type); // vo의 file type을 저장
				
				vo.setExtension(contentType);
				System.out.println(contentType);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			String id = vo.getId(); // vo id셋팅해주기
			service.registerContent(vo); // db저장
			service.registerFile(vo); // db저장
			logger.info("file넣음");
			Integer[] dnoes = vo.getDnos();
			String pw = vo.getPw();
			
			for (int i = 0; i < dnoes.length; i++) {
				logger.info("order넣으러 왔다.");
				service.registerOrder(id, dnoes[i],pw); // db저장
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:/order/list"; // 메세지로 성공과 실패를 알 수 있다. // 요기 나중에 페이징 처리된

	}

	@RequestMapping(value = "/device")
	public ResponseEntity<List<DeviceVO>> deviceList() throws Exception {
		logger.info("divice list");
		List<DeviceVO> list = dservice.listDevice();

		// return new ResponseEntity<List<TodoVO>>(listall,HttpStatus.OK);
		return new ResponseEntity<List<DeviceVO>>(list, HttpStatus.OK);
	}

}
