package com.before5.controller;



import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.before5.domain.MemberVO;
import com.before5.service.JoinService;

@Controller
@RequestMapping("/member/*")
public class JoinController {

	private static final Logger logger = LoggerFactory.getLogger(JoinController.class);

	@Inject
	public JoinService service;

	@RequestMapping(value = "/join", method = RequestMethod.GET)
	public void joinGet() throws Exception {
		logger.info("회원가입 폼 들어옴");
	}

	@RequestMapping(value = "/join", method = RequestMethod.POST)
	public ResponseEntity<String> joinPost(@RequestBody MemberVO vo) throws Exception {
		
		ResponseEntity<String> entity = null;

		logger.info("버튼누름");
		vo.setGrade(3);
		logger.info(vo.toString());
		try {
			service.joinMember(vo);
			entity = new ResponseEntity<String>("SUCCESS", HttpStatus.BAD_REQUEST.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	
	@RequestMapping(value="/check/{id}", method=RequestMethod.GET)
	public ResponseEntity<String> checkGet(@PathVariable("id") String id ) throws Exception{
		
		logger.info("id check");
		if(service.checkId(id)== null){
			return new ResponseEntity<String>("nofind",HttpStatus.OK);
		}
		return new ResponseEntity<String>("find",HttpStatus.OK);
		
	}
	
}
