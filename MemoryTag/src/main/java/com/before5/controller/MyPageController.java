package com.before5.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.before5.domain.Criteria;
import com.before5.domain.FileVO;
import com.before5.domain.LoginVO;
import com.before5.domain.MyPageVO;
import com.before5.domain.PageMaker;
import com.before5.service.MyPageService;
import com.before5.util.MediaUtils;
import com.before5.util.UploadFileUtils;


@Controller
@RequestMapping("/mypage/*")
public class MyPageController {

	private static final Logger logger = LoggerFactory.getLogger(MyPageController.class);

	@Inject
	public MyPageService service;

	@RequestMapping(value = "/myorder", method = RequestMethod.GET)
	public void myOrder(@ModelAttribute("cri") Criteria cri, Model model, HttpSession session) throws Exception {

		logger.info("마이페이지에 들어왔다");
		logger.info(cri.toString()); // cri 한번 출력 해보구

		Object user = session.getAttribute("login");
		LoginVO login = (LoginVO) user;
		String id = login.getId(); // 로그인 한 아이디를 가지고 온다.

		// List<MyPageVO> list = service.myOrder(id);
		// model.addAttribute("myorderlist",list); // 내 아이디로 검색한 모든 리스트

		PageMaker pageMaker = new PageMaker();// 페이지 메이커 만들고
		pageMaker.setCri(cri);
		pageMaker.setTotalCount(service.countOrder(id)); // id로 검색한 모든 리스트들의 숫자

		List<MyPageVO> orderList = service.myListOrder(id, cri); // 리스트

		model.addAttribute("myorderlist", orderList);
		model.addAttribute("pageMaker", pageMaker);
		// model에 모두 넣어서 보낸다.
	}

	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public void orderReadGet(@RequestParam("ono") int ono, @ModelAttribute("cri") Criteria cri, Model model,
			HttpSession session) throws Exception {

		logger.info("마이페이지 read");

		Object user = session.getAttribute("login");
		LoginVO login = (LoginVO) user;
		String id = login.getId(); // 로그인 한 아이디를 가지고 온다.

		MyPageVO vo = new MyPageVO();
		vo = service.readOrder(id, ono);

		model.addAttribute("oneOrder", vo); // 내 orderlist를 보내준다.
		System.out.println(vo);
		FileVO file = new FileVO();
		file = service.readFile(vo.getFno());

		String originalName = file.getFilename();
		String directory = file.getDirectory();
		file.setFilename(directory + File.separator + originalName);
		// "s_"
		file.setThumnail(directory + File.separator + "s_" + originalName);

		String[] original = originalName.split("_");
		String origin = original[1];
		System.out.println(origin);
		file.setOriginal(origin);
		System.out.println(file);
		model.addAttribute("file", file);


	}

	@ResponseBody
	@RequestMapping(value = "/file")
	public ResponseEntity<byte[]> displayFile(String fileName) throws Exception {

		InputStream in = null; // inputstream
		ResponseEntity<byte[]> entity = null;
		logger.info("FILE NAME : " + fileName);

		try {
			String formatName = fileName.substring(fileName.lastIndexOf(".") + 1);

			MediaType mType = MediaUtils.getMediaType(formatName);

			HttpHeaders headers = new HttpHeaders();
			in = new FileInputStream(fileName);

			if (mType != null) {
				headers.setContentType(mType);
			}
			fileName = fileName.substring(fileName.indexOf("_") + 1);
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.add("Content-Disposition",
					"attachment; filename=\"" + new String(fileName.getBytes("UTF-8"), "ISO-8859-1") + "\"");

			entity = new ResponseEntity<byte[]>(IOUtils.toByteArray(in), headers, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
		} finally {
			in.close();
		}
		return entity;
	}

	@RequestMapping(value = "/modifyText", method = RequestMethod.GET)
	public void modifyTextGet(Integer cno, @ModelAttribute("cri") Criteria cri, Model model) throws Exception {
		logger.info("modifyText Get..");

		model.addAttribute(service.oneContent(cno));
	}

	@RequestMapping(value = "/modifyText", method = RequestMethod.POST)
	public String modifyTextPost(MyPageVO vo, @ModelAttribute("cri") Criteria cri) throws Exception {

		logger.info("modifyText post....");
		System.out.println(vo);

		// service.modifyText(vo);
		service.updateContent(vo);
		System.out.println("됬나??");
		int page = cri.getPage();
		int ono = vo.getOno();

		return "redirect:/mypage/read?page=" + page + "&ono=" + ono;
	}

	@RequestMapping(value = "/modifyFile", method = RequestMethod.GET)
	public void modifyFileGet(MyPageVO mypage,int fno, @ModelAttribute("cri") Criteria cri, Model model) throws Exception {
		FileVO vo = new FileVO();
		vo = service.readFile(fno);
		System.out.println(mypage);
		
		model.addAttribute("mypage",mypage);
		model.addAttribute("filevo",vo); // fileVO model에 담아서 넘겨준다. 
		
		System.out.println(vo);
		String directory = vo.getDirectory();
		String filename = vo.getFilename();
		String path = directory + File.separator+ filename;
		System.out.println(path);
		
		// 파일이 이미지 파일인지 아닌지 판별한 후에
		if (MediaUtils.getMediaType(vo.getType()) != null) {
			// 이미지 파일이면 썸네일까지 삭제한다.
			String spath = directory + File.separator+"s_"+ filename;
			System.out.println(spath);
			File file = new File(spath);
			if (file.exists() == true) {
				file.delete();
			}
		}

		// 파일을 삭제한다.
		File file = new File(path);
		if (file.exists() == true) {
			file.delete();
		}
		
		
	}
	
	@RequestMapping(value="/modifyFile", method = RequestMethod.POST)
	public String modifyFilePost(@RequestParam("file") MultipartFile[] files,@RequestParam("fno") int fno){
		
		logger.info("modifyFile post.......");
		System.out.println(fno); // 잘 가져오네 
		
		FileVO vo = new FileVO();
		vo.setFno(fno);
		
		//디렉토리 만들자.
		Calendar cal = Calendar.getInstance(); // 날짜를 가져온다.
		String year = File.separator + cal.get(Calendar.YEAR); // 구분자+년도=1
		
		String month = year + File.separator + new DecimalFormat("00").format(cal.get(Calendar.MONTH) + 1); // 년도
																											// +
																											// 구분자+월
		String date = month + File.separator + new DecimalFormat("00").format(cal.get(Calendar.DATE)); //
		// 년도 + 구분자+월+일
		logger.info(date + File.separator);
		String makeDir = "C:\\zzz\\" + date + File.separator; //

		vo.setDirectory("C:\\zzz\\" + date); // 디렉토리 셋팅 끝
		File dir = new File(makeDir);
		
		if (dir.exists() == false) {
			dir.mkdirs();
		} // 폴더가 없으면 만들고
		
		for (MultipartFile file : files) {
			UUID uid = UUID.randomUUID();
			String savedName = uid.toString() + "_" + file.getOriginalFilename(); //savedname = uuid+_+originalname

			Integer size = (int) file.getSize();
			vo.setF_size(size);
			File target = new File(makeDir + savedName); // 요기에 파일을 만들어야 한다.
			
			//public static String makeThumbnail(String uploadPath, String path, String fileName)

			
			try {
				
				FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(target));
				vo.setFilename(savedName); // vo의 fileName을 savedName으로 저장해준다.
				String[] name = savedName.split("\\."); // 정규 표현식 때문에 .은 //.으로
														// 인식해야 한다.
				String type = name[1];// 확장자
				System.out.println(type);
				String contentType = file.getContentType();
				
				if(MediaUtils.getMediaType(type) != null){
					
					String thumnail = UploadFileUtils.makeThumbnail("C:\\zzz\\", date, savedName);
					// 썸네일 만들어야 한다.
					System.out.println("thumnail"+thumnail+"썸네일 만들었다."); // 썸네일 생성완료!!
				}
				

				vo.setType(type); // vo의 file type을 저장
				
				vo.setExtension(contentType);
				System.out.println(contentType);
				
				service.updateFile(vo);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public ResponseEntity<String> deleteOrder(@RequestParam("list") String[] list) throws Exception{
		
		
		for(int i=0;i<list.length;i++){
			System.out.println(list[i]);
			int ono = Integer.parseInt(list[i]);
			System.out.println(ono);
			service.deleteOrder(ono);
		}
		
		return new ResponseEntity<String>("SUCCESS",HttpStatus.OK);
	}


}
