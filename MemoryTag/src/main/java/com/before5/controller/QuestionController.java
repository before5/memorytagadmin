package com.before5.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import com.before5.domain.Criteria;
import com.before5.domain.LoginVO;
import com.before5.domain.PageMaker;
import com.before5.domain.QuestionVO;
import com.before5.service.QuestionService;

@Controller
@RequestMapping("/question/*")
public class QuestionController {
	
	private static final Logger logger = LoggerFactory.getLogger(QuestionController.class);
	
	@Inject
	public QuestionService service;  // service 객체
	
	
	@RequestMapping(value ="/list", method=RequestMethod.GET)
	public void listGET(@ModelAttribute("cri") Criteria cri,Model model) throws Exception{
		
		logger.info(cri.toString()); 
		
		model.addAttribute("list",service.pageList(cri)); // 현재 page를 가지고 있는 cri로 현재 page에 맞는 data를 가져올꺼다.
		
		PageMaker pageMaker = new PageMaker(); // pageMaker을 넘겨 주어야 하므로 pageMaker객체 생성
		pageMaker.setCri(cri); // pageMaker의 cri를 셋팅 (현재 페이지를 넘겨준다)
		pageMaker.setTotalCount(service.getTotalCount());// 전체 데이터수를 넘겨서 pageMaker를 setting!!
		
		model.addAttribute("pageMaker",pageMaker); // jsp파일에 pageMaker를 보내준다.
		
	} // 모든 공지사항 list를 보여줄 method
	
	@RequestMapping(value="/read", method=RequestMethod.GET)
	public void readPage(@RequestParam("qno") int qno,@ModelAttribute("cri") Criteria cri, Model model,HttpSession session) throws Exception{
		
		logger.info("read Q&A.......");
		Object user = session.getAttribute("login"); 
		LoginVO login = (LoginVO) user;
		String id = login.getId(); // 로그인 한 아이디를 가지고 온다.
		System.out.println(id);
		model.addAttribute("id",id); // model에 넣어준다. 
		
		model.addAttribute(service.readOneQuestion(qno));
		
	} // 하나의 공지사항을 보여줄 method
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public String removePOST(@ModelAttribute("cri") Criteria cri, QuestionVO vo, Model model) throws Exception{
		logger.info("remove post");
		System.out.println("들어왔남");
		System.out.println(vo.toString());
		service.deleteQuestion(vo.getQno());
		return "redirect:/question/list?page="+cri.getPage();
	}
}
