package com.before5.controller;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.before5.domain.Criteria;
import com.before5.domain.PageMaker;
import com.before5.domain.SearchCriteria;
import com.before5.service.OrderService;

@Controller
@RequestMapping("/order")
public class OrderController {

	@Inject
	private OrderService service;

	// 전체조회 메소드(listAll)
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	public void ListAll(Model model) throws Exception {

		model.addAttribute("list", service.listAll());

	}

	// 삭제 메소드(remove)
	@RequestMapping(value = "/remove", method = RequestMethod.POST)
	public String remove(@RequestParam("ono") Integer ono, RedirectAttributes rttr) throws Exception {

		service.remove(ono);

		rttr.addFlashAttribute("msg", "삭제되었습니다");
		return "redirect:/order/list";
	}

	// 전체조회 메소드(listCri)필요없고
	@RequestMapping(value = "/listCri", method = RequestMethod.GET)
	public void ListCri(Criteria cri, Model model) throws Exception {

		model.addAttribute("list", service.listCriteria(cri));

	}

	//필요없음.
	@RequestMapping(value = "/listPage", method = RequestMethod.GET)
	public void listPage(@ModelAttribute("cri")Criteria cri, Model model) throws Exception {

		System.out.println(service.listCountCriteria(cri));
		model.addAttribute("list", service.listCriteria(cri));
		PageMaker pageMaker = new PageMaker(); // pageMaker 생성
		pageMaker.setCri(cri); // cri에는 page와 perPageNum이 있음
		// pageMaker.setTotalCount(28); //전에는 totalCount를 직접 입력해야했음

		pageMaker.setTotalCount(service.listCountCriteria(cri)); // 실제
																	// totalcount
																	// 를 입력받음

		model.addAttribute("pageMaker", pageMaker); // pageMaker를 model 객체에 담음
	}
	
	
}
