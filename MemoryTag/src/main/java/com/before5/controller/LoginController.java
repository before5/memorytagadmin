package com.before5.controller;

import java.util.Date;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.WebUtils;

import com.before5.domain.LoginVO;
import com.before5.service.LoginService;

@Controller
@RequestMapping("/user")
public class LoginController {					//로그인을 위한 컨트롤러 입니다.

	@Inject
	private LoginService service;				//service를 주입합니다.
	
	@RequestMapping(value="/login", method=RequestMethod.GET)//GET방식으로 올때를 설정합니다.
	public void loginGET(@ModelAttribute("vo")LoginVO vo){	//@ModelAttribute는 자동으로 해당 객체를 뷰까지 전달하도록 만드는 애노테이션입니다.
		System.out.println("겟 로그인");
	}
	
	@RequestMapping(value="/reLogin", method=RequestMethod.GET)//GET방식으로 올때를 설정합니다.
	public void reLogin(@ModelAttribute("vo")LoginVO vo){	//@ModelAttribute는 자동으로 해당 객체를 뷰까지 전달하도록 만드는 애노테이션입니다.
		System.out.println("겟 로그인");
	}
	
	@RequestMapping(value="/loginPost", method=RequestMethod.POST)//POST방식입니다.
	public void loginPOST(LoginVO vo,Model model, HttpSession session)throws Exception{
		System.out.println("포스트로 받음");
		System.out.println(vo);
		LoginVO successVO = service.login(vo);						//service.login(vo)메소드로 DB에 있는 vo를 가져옵니다.그후에 successVO에 집어넣습니다.
		if(successVO == null){										//successVO를 받아오지 못한다면 다시하도록 하기
			model.addAttribute("msg","ID or PW가 틀림");
			return;
		}
		
		
		if(successVO.getGrade() != 1){								//등급이 1이 아니라면 로그인이 되지 않습니다.
			return;
		}
		
		model.addAttribute("LoginVO",successVO);					//View로 successVO을 보냅니다.
		if(vo.isUseCookie()){										//Remember Me를 사용하면 쿠키를 생성합니다.
			int amount = 60*60*24;									//60초*60분*24시간을 곱해서 저장합니다.
			Date sessionLimit = new Date(System.currentTimeMillis()+(1000*amount));//현재 시간+1000*amount을 저장합니다.
			service.keepLogin(vo.getId(), session.getId(), sessionLimit);//그런후에 DB에 sessionKey 와sessionLimit을 저장합니다.
			//
		}
		System.out.println("모든걸 다함");
	}
	
	@RequestMapping(value = "/logout",method = RequestMethod.GET)
	public void logout(HttpServletRequest req, HttpServletResponse res, HttpSession session)throws Exception{
		
		Object obj = session.getAttribute("login");
		
		
		
		if(obj != null){
			LoginVO vo = (LoginVO) obj;
			
			session.removeAttribute("login");
			session.invalidate();
			
			Cookie loginCookie = WebUtils.getCookie(req, "loginCookie");
			
			if(loginCookie!= null){
				
				loginCookie.setPath("/");
				loginCookie.setMaxAge(0);
				res.addCookie(loginCookie);
				service.keepLogin(vo.getId(), session.getId(), new Date());
			}
			
		}
			
	}
	
}
