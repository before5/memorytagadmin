package com.before5.controller;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.before5.domain.Criteria;
import com.before5.domain.MemberVO;
import com.before5.domain.PageMaker;
import com.before5.domain.SearchCriteria;
import com.before5.service.MemberService;

@Controller
@RequestMapping("/admin") 
public class MemberController {
	
	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
	
	@Inject
	MemberService service;
	
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String admin(Model model) {
		
		model.addAttribute("list",service.readAllMember());
		
		return "admin";
	}
	
	@RequestMapping(value="/read",method=RequestMethod.GET)			//조회처리용 페이지 GET방식
	public void read(@RequestParam("id")String id, Model model)throws Exception{
		model.addAttribute(service.readMember(id));					//model.addAttribute로 service.readMember(id)를 보냅니다. 
	}
	
	@RequestMapping(value="/listCri", method = RequestMethod.GET)
	public void listAll(Criteria cri, Model model)throws Exception{
		logger.info("Criteria페이지가 보입니다.");
		
		model.addAttribute("list",service.listCriteria(cri));
	}
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public void listPage(@ModelAttribute("cri")SearchCriteria cri, Model model)throws Exception{
		
		logger.info(cri.toString());
		
		//model.addAttribute("list",service.listCriteria(cri));
		model.addAttribute("list",service.listSearchCriteria(cri));
		
		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(cri);
		//pageMaker.setTotalCount(131);
		
		//pageMaker.setTotalCount(service.countPaging(cri));
		pageMaker.setTotalCount(service.listSearchCount(cri));
		model.addAttribute("pageMaker",pageMaker);
	}
	
	@RequestMapping(value = "/remove", method = RequestMethod.POST)	//삭제를 위한 메소드 입니다.
	public String remove(@RequestParam("id") String[] ids,
			SearchCriteria cri, RedirectAttributes rttr) throws Exception {
		//JSP에서ㅏ ID를 Sring형식으로 넘겨 받을 겁니다.
		
		for (String  id: ids) {
			service.deleteMember(id);//넘겨받은 id를 가지고 서비스의 메소드를 실행시킵니다.
			}
				
		rttr.addAttribute("page", cri.getPage());//redirect할떄 값들을 같이 전송하기위해서 씀 page값을 전송함
		rttr.addAttribute("perPageNum", cri.getPerPageNum());//perPageNum전송하기 위해서 씀
		rttr.addAttribute("searchType", cri.getSearchType());//혹시 검색을 했다면 무슨타입으로 검색했는지를 고대로 전송함
		rttr.addAttribute("keyword", cri.getKeyword());//검색한 검색명을 전송하기 위해서 사용

		rttr.addFlashAttribute("msg", "삭제 완료");	//이모든 것을 msg에 담아서 보내줌 JSP에게 보내준다. 그럼 4가지 값을 가지고 새로운페이지를 새로 펼친다.
		return "redirect:/admin/list";
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public ResponseEntity<String> edit(@RequestBody String vo,
			RedirectAttributes rttr) throws Exception {
		System.out.println("뭔가옴");
		System.out.println(vo);
		ResponseEntity<String> entity = null;

		String[] parse=vo.split("&");	//넘어오는 데이터가 정상이아니라 파싱해야함
					//young1638390=1&young16192544=2&sang48224576=1
		
		for (String string : parse) {		//&파싱하고 나서도 =으로 id와 pw로 나눠야합니다.

			String[] param=string.split("=");
			//param[0] 아이디
			//param[1] 수정할 등급 이렇게 있습니다
			
			MemberVO updateVO = new MemberVO();	//쿼리문에 필요한 updateVO를 만듭니다.
			updateVO.setId(param[0]);		//param[0]은 id이므로 updateVO의 id값을 넣습니다.
			updateVO.setGrade(Integer.parseInt(param[1]));//param[1]은 grade이므로 updateVO의 grade값을 넣습니다.			
			service.updateMember(updateVO);	//가져온 값을 가지고 service.updateMember메소드를 실행합니다.
		}	
		
		try {
			entity = new ResponseEntity<String>("SUCCESS", HttpStatus.BAD_REQUEST.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		
		return entity;
	}
}
