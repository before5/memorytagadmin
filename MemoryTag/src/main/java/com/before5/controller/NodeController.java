package com.before5.controller;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.before5.domain.NodeVO;
import com.before5.service.NodeService;

@RestController
@RequestMapping("/node")
public class NodeController {

	// 노드에서 요청이 들어오면 보내줄꺼다!!
	private static final Logger logger = LoggerFactory.getLogger(NodeController.class);
	
	@Inject
	public NodeService service;
	
	@RequestMapping(value="/select", method=RequestMethod.GET)
	public ResponseEntity<List<NodeVO>> selectGet(){
		logger.info("node controller 들어왔당 ");
		List<NodeVO> list = service.select();
		
		//return new ResponseEntity<List<TodoVO>>(listall,HttpStatus.OK);
		return new ResponseEntity<List<NodeVO>>(list,HttpStatus.OK); 
	}
	
}
