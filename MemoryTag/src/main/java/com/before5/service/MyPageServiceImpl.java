package com.before5.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.before5.domain.Criteria;
import com.before5.domain.FileVO;
import com.before5.domain.MyPageVO;
import com.before5.persistence.MyPageDAO;

@Service
public class MyPageServiceImpl implements MyPageService {

	@Inject 
	private MyPageDAO dao;
	@Override
	public List<MyPageVO> myOrder(String id) throws Exception{
		
		return dao.myOrder(id);
	}
	@Override
	public List<MyPageVO> myListOrder(String id, Criteria cri) throws Exception {
		// TODO Auto-generated method stub
		return dao.myListOrder(id, cri);
	}
	@Override
	public int countOrder(String id) throws Exception {
		// TODO Auto-generated method stub
		return dao.countOrder(id);
	}
	@Override
	public MyPageVO readOrder(String id, int ono) throws Exception {
		
		return dao.readOrder(id, ono);
	}
	@Override
	public FileVO readFile(int fno) throws Exception {
		// TODO Auto-generated method stub
		return dao.readFile(fno);
	}
	
	@Override
	public MyPageVO oneContent(int cno) throws Exception {
		// TODO Auto-generated method stub
		return dao.oneContent(cno);
	}
	@Override
	public void updateContent(MyPageVO vo) throws Exception {
		dao.updateContent(vo);
		
	}
	@Override
	public void updateFile(FileVO vo) throws Exception {
		// TODO Auto-generated method stub
		dao.updateFile(vo);
	}
	@Override
	public void deleteOrder(int ono) throws Exception {
		// TODO Auto-generated method stub
		dao.deleteOrder(ono);
	}

}
