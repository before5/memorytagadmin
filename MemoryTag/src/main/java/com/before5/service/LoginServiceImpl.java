package com.before5.service;

import java.util.Date;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.before5.domain.LoginVO;
import com.before5.persistence.LoginDAO;

@Service					//Service에 쓰는 어노테이션 @Service
public class LoginServiceImpl implements LoginService{	//LoginService를 상속받는다.

	@Inject
	LoginDAO dao;				//LoginDAO와 연결이 되므로 주입한다.

	@Override					
	public LoginVO login(LoginVO vo) throws Exception {		//login 메소드를 사용한다.(오버라이드 받아서)
		
		return dao.login(vo);				//dao값을 리턴받음으로 인하여 dao값을 가져온다.
	}

	@Override
	public void keepLogin(String id, String sessionId, Date next) throws Exception {

		dao.keepLogin(id, sessionId, next);		//dao에서 keepLogin메소드를 작동시켜서 id, sessionId, next데이터를 넘김니다.
	}

	@Override
	public LoginVO checkLoginWithSessionKey(String value) {
		
		return dao.checkLoginWithSessionKey(value);//dao에서 checkLoginWithSessionKey를 실행시킨값을 리턴값으로 받습니다.
	}
	
	
}
