package com.before5.service;



import com.before5.domain.MemberVO;

public interface JoinService {

	public void joinMember(MemberVO vo) throws Exception;
	
	public MemberVO checkId(String id) throws Exception;
}
