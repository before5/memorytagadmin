package com.before5.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.before5.domain.Criteria;
import com.before5.domain.QReplyVO;
import com.before5.persistence.QReplyDAO;

@Service
public class QReplyServiceImpl implements QReplyService{

	@Inject 
	public QReplyDAO dao;

	@Override
	public List<QReplyVO> allReply(int qno) throws Exception {
		// TODO Auto-generated method stub
		return dao.allReply(qno);
	}

	@Override
	public void addReply(QReplyVO vo) throws Exception{
		// TODO Auto-generated method stub
		dao.addReply(vo);
	}

	@Override
	public void modifyReply(QReplyVO vo) throws Exception{
		// TODO Auto-generated method stub
		dao.modifyReply(vo);
	}

	@Override
	public void deleteReply(int rno) throws Exception{
		// TODO Auto-generated method stub
		dao.deleteReply(rno);
	}

	@Override
	public int countReply(int qno) throws Exception{
		// TODO Auto-generated method stub
		return dao.countReply(qno);
	}

	@Override
	public List<QReplyVO> pageReply(Criteria cri, int qno) throws Exception {
		// TODO Auto-generated method stub
		return dao.pageReply(cri, qno);
	}
}
