package com.before5.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.before5.domain.Criteria;
import com.before5.domain.QuestionVO;
import com.before5.persistence.QuestionDAO;

@Service
public class QuestionServiceImpl implements QuestionService {

	@Inject 
	QuestionDAO dao; // dao 객체 
	
	@Override
	public List<QuestionVO> readAllQuestion() throws Exception {
		// TODO Auto-generated method stub
		return dao.readAllQuestion();
	}

	@Override
	public QuestionVO readOneQuestion(Integer qno) throws Exception {
		// TODO Auto-generated method stub
		return dao.readOneQuestion(qno);
	}

	@Override
	public void deleteQuestion(Integer qno) throws Exception{
		// TODO Auto-generated method stub
		dao.deleteQuestion(qno);
	}

	@Override
	public int getTotalCount()throws Exception {
		// TODO Auto-generated method stub
		return dao.totalCount();
	}

	@Override
	public List<QuestionVO> pageList(Criteria cri) throws Exception {
		// TODO Auto-generated method stub
		return dao.pageList(cri);
	}

}
