package com.before5.service;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.QuestionVO;

public interface QuestionService {

	public List<QuestionVO> readAllQuestion() throws Exception; // 모든 공지사항의 list를 가져오는 method
	
	public QuestionVO readOneQuestion(Integer qno) throws Exception; //하나만 클릭했을 때 하나의 공지사항을 가져오는 method 
	
	public void deleteQuestion(Integer qno) throws Exception; // 공지사항을 삭제하는 method
	
	public int getTotalCount() throws Exception;// 전체 데이터 수를 조회하는 method
	
	public List<QuestionVO> pageList(Criteria cri) throws Exception; // page에 맞는 list를 가져오는 method
}
