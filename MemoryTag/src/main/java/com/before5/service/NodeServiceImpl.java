package com.before5.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.before5.domain.NodeVO;
import com.before5.persistence.NodeDAO;

@Service
public class NodeServiceImpl implements NodeService {

	@Inject
	public NodeDAO dao;
	@Override
	public List<NodeVO> select() {
		// TODO Auto-generated method stub
		return dao.select();
	}

}
