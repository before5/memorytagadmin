package com.before5.service;

import java.util.List;

import com.before5.domain.NodeVO;

public interface NodeService {

	public List<NodeVO> select();
}
