package com.before5.service;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.FileVO;
import com.before5.domain.MyPageVO;

public interface MyPageService {

	public List<MyPageVO> myOrder(String id) throws Exception;
	
	public List<MyPageVO> myListOrder(String id,Criteria cri) throws Exception;
	
	public int countOrder(String id) throws Exception;
	
	public MyPageVO readOrder(String id,int ono) throws Exception;
	
	public FileVO readFile(int fno) throws Exception;
	
	public void updateContent(MyPageVO vo) throws Exception;
	
	public MyPageVO oneContent(int cno) throws Exception;
	
	public void updateFile(FileVO vo) throws Exception;
	
	public void deleteOrder(int ono) throws Exception;
}
