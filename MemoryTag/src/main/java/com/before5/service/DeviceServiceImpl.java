package com.before5.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.before5.domain.DeviceVO;
import com.before5.persistence.DeviceDAO;

@Service
public class DeviceServiceImpl implements DeviceService{

	@Inject
	private DeviceDAO dao;

	@Override
	public List<DeviceVO> listDevice() {
		
		return dao.list();
	}
	
	
}
