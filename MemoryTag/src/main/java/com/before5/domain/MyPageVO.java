package com.before5.domain;

import java.util.Date;

public class MyPageVO {
	
	private String title;
	private Date regdate;
	private Date orderdate;
	private String text;
	private Integer fno;
	private String fileName;
	private Integer ono;
	private String id;
	private Integer dno;
	private Integer cno;
	private String pw;
	private int fontsize;
	private String fontcolor;
	

	public int getFontsize() {
		return fontsize;
	}
	public void setFontsize(int fontsize) {
		this.fontsize = fontsize;
	}
	public String getFontcolor() {
		return fontcolor;
	}
	public void setFontcolor(String fontcolor) {
		this.fontcolor = fontcolor;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(Date orderdate) {
		this.orderdate = orderdate;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Integer getFno() {
		return fno;
	}
	public void setFno(Integer fno) {
		this.fno = fno;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Integer getOno() {
		return ono;
	}
	public void setOno(Integer ono) {
		this.ono = ono;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getDno() {
		return dno;
	}
	public void setDno(Integer dno) {
		this.dno = dno;
	}
	public Integer getCno() {
		return cno;
	}
	public void setCno(Integer cno) {
		this.cno = cno;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	@Override
	public String toString() {
		return "MyPageVO [title=" + title + ", regdate=" + regdate + ", orderdate=" + orderdate + ", text=" + text
				+ ", fno=" + fno + ", fileName=" + fileName + ", ono=" + ono + ", id=" + id + ", dno=" + dno + ", cno="
				+ cno + ", pw=" + pw + ", fontsize=" + fontsize + ", fontcolor=" + fontcolor + "]";
	}
	
	
	
	
}
