package com.before5.domain;

import java.util.Date;

//공지사항 객체
public class QuestionVO {

	private Integer qno;
	private String title;
	private String content;
	private String id;
	private Date regdate;
	private Date updatedate;
	
	
	public QuestionVO() {
		super();
	}
	
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public Integer getQno() {
		return qno;
	}
	public void setQno(Integer qno) {
		this.qno = qno;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}
	@Override
	public String toString() {
		return "QuestionVO [qno=" + qno + ", title=" + title + ", content=" + content + ", regdate=" + regdate
				+ ", updatedate=" + updatedate + "]";
	}

	
	
}
