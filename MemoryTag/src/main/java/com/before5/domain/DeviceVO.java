package com.before5.domain;

public class DeviceVO {
	
	private Integer dno;
	private double lat;
	private double lng;
	private int dsize;
	
	public Integer getDno() {
		return dno;
	}
	public void setDno(Integer dno) {
		this.dno = dno;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public int getDsize() {
		return dsize;
	}
	public void setDsize(int dsize) {
		this.dsize = dsize;
	}
	
	@Override
	public String toString() {
		return "DeviceVO [dno=" + dno + ", lat=" + lat + ", lng=" + lng + ", dsize=" + dsize + "]";
	}
	
	
	

}
