package com.before5.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class LoginInterceptor extends HandlerInterceptorAdapter {	//인터셉터를 쓰기위하여
																	//HandlerInterceptorAdapter를 연장합니다.
	
	private static final String LOGIN = "login";	//LOGIN이라는 스트링 객체에 login이라고 
													//값을 집어 넣습니다.(일종의 초기화 같군요.)
	private static final Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);
	
													//Logger를 쓸게요 syso지겹자나요.
	
	
	@Override
	public void postHandle(HttpServletRequest req, HttpServletResponse res, 
			Object handler, ModelAndView modelAndView)throws Exception{
																		//postHandle의 파라미터로 	HttpServletRequest(request를 하기위해),HttpServletResponse
																		//(response를 쓰기위해서),Object handler(무슨 이벤트가 발생하면 감지하기 위하여 사용하는것 같습니다.
																		//정확히는 모르겠지만 한가지 확실한 것은 지우면 에러가 납니다.), ModelAndView(하나의 객체에 Model데이터와
																		//View의 처리를 동시에 하는 객체 입니다. Model파라미터로 쓸수 없기에 대용합니다.)들을 사용합니다.
//		logger.info("로그인saddddddddddddddddddd 성공입니다.");	
//		System.out.println("인터셉터 post들어옴");
		HttpSession session = req.getSession();				//session을 HttpServletRequest로 씁니다.
		ModelMap modelMap = modelAndView.getModelMap();		//modelAndView에서 Model값을 뽑아서
															//modelMap에 집어 넣습니다.
		Object LoginVO = modelMap.get("LoginVO");			//LoginVO에 modelMap의 "LoginVO"를 넣습니다. 
		
		if(LoginVO != null){								//LoginVO가 무엇인가 있다면
//			System.out.println("로그인 성공");
//			logger.info("로그인 성공입니다.");	
			session.setAttribute(LOGIN, LoginVO);			//LOGIN이란 이름으로 LoginVO를 설정합니다. 

			
			if(req.getParameter("useCookie") != null){		//Remember me를 눌렀는지 확인합니다.
				logger.info("자동 로그인.............");
				Cookie loginCookie = new Cookie("loginCookie",session.getId());//쿠키를 생성해줍니다.
				loginCookie.setPath("/");					//  /를 해놨기때문에 어떤 페이지에서든지 쿠키가 사용됩니다.
				loginCookie.setMaxAge(60*60*24);			//60초*60분*24시간을 곱해서 저장합니다.
				res.addCookie(loginCookie);					//쿠키를 넣어줍니다.
			}
//			res.sendRedirect("/");							//재접속하여 LOGIN된화면을 보여줍니다.  제거한 이유는 이제 SaveDest로 현재보고 있던 페이지를 저장하기 때문입니다.
			Object dest = session.getAttribute("dest");		//저장된 dest값을 추출해옵니다
			
			res.sendRedirect(dest != null ? (String)dest:"/");//dest값을 가지고 저장된 페이지로 
			
		}else{
		res.sendRedirect("/user/reLogin");	
		}
	}
	
	
	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, 
			Object handler)throws Exception{
														//위에 설명과 비슷해보입니다.	
		HttpSession session = req.getSession();
														//session을 HttpServletRequest로 씁니다.
		if(session.getAttribute(LOGIN) != null){		//세션을 통해서 LOGIN인이라는 값이 있다면
//			System.out.println("인터셉터 pre들어옴");
//			logger.info("로그인 데이터를 지웁니다.");
			session.removeAttribute(LOGIN);				//세션에 LOGIN을 지웁니다.
			session.removeAttribute("NLOGIN");
		}		 
//		System.out.println("인터셉터 pre지나감");
//		logger.info("로그인11111111111 성공입니다.");	
		return true;									//true면 이 메소드를 지나갑니다.
		
	}
}
