package com.before5.interceptor;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import com.before5.domain.LoginVO;
import com.before5.service.LoginService;

public class AuthInterceptor extends HandlerInterceptorAdapter  {

	private static final Logger logger = LoggerFactory.getLogger(AuthInterceptor.class);
																//로거를 쓰기위해서
	@Inject
	private LoginService service;
	
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		

		HttpSession session = request.getSession();			//session을 HttpServletRequest의 session으로 사용합니다.
		
		if(session.getAttribute("login") == null){			//session에서 login인 값이 비어있다면
			
			logger.info("최근 로그인 되지 않았습니다.");
			
			saveDest(request);
			
			Cookie loginCookie = WebUtils.getCookie(request, "loginCookie");
			
			if(loginCookie != null){
				LoginVO loginVO = service.checkLoginWithSessionKey(loginCookie.getValue());
				
				logger.info("LOGINVO: "+ loginVO);
				
				if(loginVO != null){
					session.setAttribute("login", loginVO);
					return true;
				}
			}
			response.sendRedirect("/user/login");			//Login페이지로 보냅니다.
			return false;							
		}
		return true;
	}

	private void saveDest(HttpServletRequest req){			//현재보고 있는 페이지를 유지하면서 로그인 페이지로 다녀오기 위해서 현재보고 있는 페이지를 만들려고 씁니다.
		
		String uri = req.getRequestURI();					//uri를 특별한 설정없이 HttpServletRequest의 getRequestURI기능으로 사용합니다.
		
		String query = req.getQueryString();				//query또한 HttpServletRequest의 getQueryString기능으로 사용합니다.
		
		if(query == null || query.equals("null")){			//query가  비어있다면
			query="";										//초기화 시켜주고
		}else{
			query = "?" + query;							//있다면 ?+query를 만들어 줍니다.
		}
		
		if(req.getMethod().equals("GET")){					//GET방식으로 들어온다면
			logger.info("dest: " + (uri + query));
			req.getSession().setAttribute("dest", uri + query);//dest라는 값에 uri+query를 더한 값을 집어 넣습니다.
		}
	}
}
