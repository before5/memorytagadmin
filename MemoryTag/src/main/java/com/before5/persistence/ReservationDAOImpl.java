package com.before5.persistence;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.ReservationVO;

@Repository
public class ReservationDAOImpl implements ReservationDAO {

	@Inject // sqlsession사용 하기 위해서 인젝트한다.
	private SqlSession session;
	
	//content 등록
	@Override
	public void registerContent(ReservationVO vo) throws Exception{
		session.insert("com.before5.mapper.ApplicationMapper.insertContent",vo);
	}

	// file등록
	@Override
	public void registerFile(ReservationVO vo) throws Exception{
		session.insert("com.before5.mapper.ApplicationMapper.insertFile",vo);
	}

	// order 등록
	@Override
	public void registerOrder(String id, int dno,String pw) throws Exception{
		Map<String,Object> paramMap = new HashMap<>();
		paramMap.put("id",id);
		paramMap.put("dno", dno);
		paramMap.put("pw", pw);
		session.insert("com.before5.mapper.ApplicationMapper.insertOrder",paramMap);
	}

}
