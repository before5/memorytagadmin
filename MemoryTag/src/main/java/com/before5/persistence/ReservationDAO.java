package com.before5.persistence;

import com.before5.domain.ReservationVO;

public interface ReservationDAO {

	//tbl_content 등록
	public void registerContent(ReservationVO vo) throws Exception;
	
	//tbl_fno 등록
	public void registerFile(ReservationVO vo) throws Exception;
	
	//tbl_ono 등록
	public void registerOrder(String id, int dno,String pw) throws Exception;
	
	
}
