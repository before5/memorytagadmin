package com.before5.persistence;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.Criteria;
import com.before5.domain.QuestionVO;

@Repository
public class QuestionDAOImpl implements QuestionDAO {

	@Inject // sqlsession사용 하기 위해서 인젝트한다.
	private SqlSession session;
	
	// 모든 공지사항을 가져오는 method
	@Override
	public List<QuestionVO> readAllQuestion() throws Exception {
		return session.selectList("com.before5.mapper.QuestionMapper.selectQuestion");
	}
	
	//qno로 하나의 공지사항에 대해 조회하는 method
	@Override
	public QuestionVO readOneQuestion(Integer qno) throws Exception{
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.QuestionMapper.selectOne",qno);
	}

	//qno로 공지사항을 삭제하는 method
	@Override
	public void deleteQuestion(Integer qno) throws Exception {
		// TODO Auto-generated method stub
		session.delete("com.before5.mapper.QuestionMapper.delete",qno);
	}

	@Override
	public int totalCount() throws Exception{
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.QuestionMapper.countTotal");
	}

	@Override
	public List<QuestionVO> pageList(Criteria cri) throws Exception{
		// TODO Auto-generated method stub
		return session.selectList("com.before5.mapper.QuestionMapper.selectPageList",cri);
	}


}
