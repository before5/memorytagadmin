package com.before5.persistence;

import java.util.List;

import com.before5.domain.NodeVO;

public interface NodeDAO {

	// Node한테 전달해 줄 객체를 select한다.
	public List<NodeVO> select();
}
