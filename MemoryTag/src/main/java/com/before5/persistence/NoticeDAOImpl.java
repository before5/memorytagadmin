package com.before5.persistence;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.Criteria;
import com.before5.domain.NoticeFileVO;
import com.before5.domain.NoticeVO;
import com.before5.domain.SearchCriteria;
@Repository
public class NoticeDAOImpl implements NoticeDAO {

	@Inject
	private SqlSession session;
	
	private static final String namespace = "com.before5.mapper.NoticeMapper";
	
	@Override
	public void create(NoticeVO vo) throws Exception {
		session.insert(namespace+".create", vo);

	}

	@Override
	public NoticeVO read(Integer nno) throws Exception {
		
		return session.selectOne(namespace+".read", nno);
	}

	@Override
	public void update(NoticeVO vo) throws Exception {
		session.update(namespace+".update", vo);

	}

	@Override
	public void delete(Integer nno) throws Exception {
		session.delete(namespace+".delete", nno);

	}

//	@Override
//	public List<NoticeVO> listAll() throws Exception {
//		// TODO Auto-generated method stub
//		return session.selectList(namespace+".listAll");
//	}
	
//	@Override
//	public List<NoticeVO> listPage(int page) throws Exception {
//		
//		return session.selectList(namespace+".listPage", page );
//	}

	@Override
	public List<NoticeVO> listCriteria(Criteria cri) throws Exception {
		
		return session.selectList(namespace+".listCriteria", cri);
	}

	@Override
	public int countPaging(Criteria cri) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne(namespace+".countPaging", cri);
	}

	@Override
	public List<NoticeVO> listSearch(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return session.selectList(namespace+".listSearch", cri);
	}

	@Override
	public int listSearchCount(SearchCriteria cri) throws Exception {
		
		return session.selectOne(namespace+".listSearchCount", cri);
	}

	@Override
	public void addAttach(NoticeFileVO filevo) throws Exception {
		session.insert(namespace+".addAttach", filevo);		
	}

	@Override
	public List<String> getAttach(Integer nno) throws Exception {
		
		return session.selectList(namespace+".getAttach", nno);
	}

	
	

}
