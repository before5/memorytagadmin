package com.before5.persistence;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.NodeVO;

@Repository
public class NodeDAOImpl implements NodeDAO {

	
	@Inject // sqlsession사용 하기 위해서 인젝트한다.
	private SqlSession session;
	
	@Override
	public List<NodeVO> select() {
		//return session.selectOne("com.before5.mapper.NodeMapper.select");
		return session.selectList("com.before5.mapper.NodeMapper.select");
	}

}
