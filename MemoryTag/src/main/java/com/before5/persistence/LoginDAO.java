package com.before5.persistence;

import java.util.Date;

import com.before5.domain.LoginVO;

public interface LoginDAO {				//LoginDAO를 Interface로 만듭니다.

	public LoginVO login(LoginVO vo)throws Exception;	//로그인을 하는 메소드
	
	public void keepLogin(String id, String sessionId, Date next);//세션값을 만드는 메소드
	
	public LoginVO checkLoginWithSessionKey(String value);//세션값을 확인하는 메소드
}
