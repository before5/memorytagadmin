<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">새로운 파일을 등록해주세요</span>
			<div class="description"></div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-body">
					<input type='hidden' name='ono' value="${mypage.ono}"> 
					
					<label for="exampleInputName2">파일 등록</label>

						<form action="/reservation/reserve" enctype="multipart/form-data"
							method="POST" id="my-awesome-dropzone">
							<!-- dropzone -->

							<div class="dropzone" id="myDropzone"></div>
						</form>
						<button type="submit" class="btn btn-default" id="fileBtn">
							예약</button>
					</div>


				</div>
			</div>
		</div>

	</div>
</div>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.1/handlebars.js"></script>
<script src="/resources/bootstrap/js/dropzone.min.js"></script>
<!--  실제 지도를 그리는 Javascript API 불러오기-->
<script type="text/javascript"
	src="//apis.daum.net/maps/maps3.js?apikey=5c03d06423534a33518270664ff2e54e"></script>

<script>
var fno = ${filevo.fno };
var ono = ${mypage.ono };
var page = ${cri.page};
console.log(fno); // fno 셋팅해주고
$(document).ready(function() {

			Dropzone.formData = new FormData();

			Dropzone.options.myDropzone = {
				url : '/mypage/modifyFile',
				acceptedFiles : "image/*, video/*",
				autoProcessQueue : false,
				uploadMultiple : true,
				parallelUploads : 10,
				maxFiles : 1,//최대 파일 업로드 갯수 // 예약페이지 에서는 파일을 최대 1개만 등록할 수 있다. 
				maxFilesize : 1000,//100mb
				addRemoveLinks : true,
				init : function() {
					dzClosure = this; // dropzone인듯 
					// 예약 버튼을 눌렀을때
					$("#fileBtn").on("click",
							function(event) {
								event.preventDefault();
								event.stopPropagation();
								dzClosure.processQueue();
							});

					this.on("sendingmultiple",function(data, xhr, formData) {

										formData.append("fno",fno);
									});

					this.on("successmultiple", function() {
						/* location.href = '/board'; */
						alert("파일을 바꿨습니다.");
						location.href = '/mypage/read?page='+page+"&ono="+ono;
					});
				}

			}
});

</script>
<%@include file="../include/footer.jsp"%>