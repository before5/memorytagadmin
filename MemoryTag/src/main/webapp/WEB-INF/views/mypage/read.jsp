<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@include file="../include/header.jsp"%>
<!-- Main Content -->
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">내 주문내역 보기</span>
			<div class="description"></div>
		</div>

		<form role="form" method="get">

			<input type='hidden' name='ono' value="${oneOrder.ono}"> <input
				type='hidden' name='page' value="${cri.page}"> <input
				type='hidden' name='cno' value="${oneOrder.cno}"> <input
				type='hidden' name='fno' value="${oneOrder.fno}">
		</form>

		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-body">
						<form>
							<div class="form-group">
								<label for="exampleInputTitle">Title</label>
								<textarea class="form-control" readonly="readonly">${oneOrder.title}</textarea>
							</div>
							<div class="form-group">
								<label for="exampleInputID">text</label>
								<textarea class="form-control" rows="5" readonly="readonly">${oneOrder.text}</textarea>
							</div>
							<div>
								<label for="exampleInputID">password</label>
								<textarea class="form-control" readonly="readonly">${oneOrder.pw}</textarea>
							</div>
						</form>
					</div>
					<div class="text-right">
						<div class="card-body">
							<button type="button" class="btn btn-success" id="modifyTxtBtn">수정</button>
							<button type="button" class="btn btn-warning" id="goListBtn">돌아가기</button>
						</div>
					</div>
					<!--  content div -->

					<div class="card-body">
						<div class="form-group">
							<form enctype="multipart/form-data" id="my-awesome-dropzone">
								<label for="exampleInputFile">등록된 파일</label>

								<div class="dropzone" id="myDropzone">

									<label>
										<div>
											<c:choose>
												<c:when test="${file.extension=='image/jpeg' }">
													<img src='/mypage/file?fileName=${file.thumnail }' />
												</c:when>
												<c:when test="${file.extension=='image/gif' }">
													<img src='/mypage/file?fileName=${file.thumnail }' />
												</c:when>
												<c:when test="${file.extension=='image/png' }">
													<img src='/mypage/file?fileName=${file.thumnail }' />
												</c:when>
												<c:otherwise>
													<img src="/resources/bootstrap/img/video.JPG" />
												</c:otherwise>

											</c:choose>
										</div> <a href='/mypage/file?fileName=${file.filename }'>
											${file.original } </a>
									</label>


								</div>
								<p class="help-block"></p>
							</form>
						</div>
					</div>
					<div class="text-right">
						<div class="card-body">
							<button type="button" class="btn btn-success" id="modifyFileBtn">파일 다시 등록하기</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
<!-- Main Content -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<<<<<<< HEAD
	<script src="/resources/bootstrap/js/dropzone.min.js"></script>
=======
<script src="/resources/bootstrap/js/dropzone.min.js"></script>
<script>
	$(document).ready(function() {

		var formObj = $("form[role='form']");
		// text 수정 버튼을 누르면
		$("#modifyTxtBtn").on("click", function(event) {
			formObj.attr("action", "/mypage/modifyText");
			formObj.attr("method", "get");
			formObj.submit();
		});
		
		$("#modifyFileBtn").on("click",function(event){
			formObj.attr("action", "/mypage/modifyFile");
			formObj.attr("method", "get");
			formObj.submit();
		});
		
		$("#goListBtn").on("click",function(event){
			self.location="/mypage/myorder?page="+${cri.page };
		})
	});
</script>
>>>>>>> refs/heads/jisun2

<%@include file="../include/footer.jsp"%>