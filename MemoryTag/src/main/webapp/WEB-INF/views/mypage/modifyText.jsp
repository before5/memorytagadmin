<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">Text 수정</span>
			<div class="description"></div>

		</div>
		<!-- 시작 -->

		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-body">
						<form role="form" method="post">
							<div class="form-group">
								<input type='hidden' name='ono' value="${oneOrder.ono}"> 
								<label for="exampleInputEmail1">CNO</label> <input type="text"
									name='cno' class="form-control" value="${myPageVO.cno}"
									readonly="readonly">
							</div>
							<div class="form-group">
								<label for="exampleInputTitle">Title</label>
								<textarea class="form-control" name='title'>${myPageVO.title}</textarea>
							</div>
							<div class="form-group">
								<label for="exampleInputTitle">Text</label>
								<textarea class="form-control" name='text'>${myPageVO.text}</textarea>
							</div>
							<div class="form-group">
								<label for="exampleInputTitle">pw</label>
								<textarea class="form-control" name='pw'>${myPageVO.pw}</textarea>
							</div>
						<div class="text-right">
							<div class="card-body">
								<button type="submit" class="btn btn-warning" id="okBtn">확인</button>
								<button type="button" class="btn btn-danger" id="backBtn">취소</button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$("#okBtn").on("click", function() {
			alert("성공했습니다!!");
			formObj.submit();
		})

	});
</script>
<%@include file="../include/footer.jsp"%>