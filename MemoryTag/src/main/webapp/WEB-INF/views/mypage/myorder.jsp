<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>

<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">My Page</span>
			<div class="description"></div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">
						<div class="col-sm-12">
							<div class="sub-title">
								<span class="description"></span>
							</div>
							<table class="table table-striped">
								<thead>
									<tr>
										<th>check</th>
										<th>주문번호</th>
										<th>title</th>
										<th>주문일</th>
										<th>예약일</th>
										<th>device번호</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${myorderlist}" var="myorder">
										<tr>

											<td scope="row" width="30px">

												<div
													class="checkbox3 checkbox-success checkbox-inline checkbox-check checkbox-round  checkbox-light">
													<input type="checkbox" name="check" value="${myorder.ono }"
														id="checkbox-fa-light-${myorder.ono }"> <label
														for="checkbox-fa-light-${myorder.ono }"></label>
												</div>

											</td>
											<td>${myorder.ono}</td>
											<td><a href='read?page=${cri.page}&ono=${myorder.ono}'>${myorder.title}</a></td>
											<td><fmt:formatDate pattern="yyyy-MM-dd"
													value="${myorder.regdate}" /></td>
											<td><fmt:formatDate pattern="yyyy-MM-dd HH"
													value="${myorder.orderdate}" /></td>
											<td>${myorder.dno }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<button type="button" class="btn btn-warning" id="deleteBtn">삭제</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="text-center">
	<ul class="pagination">
		<c:if test="${pageMaker.prev }">
			<li class="paginate_button previous" id="DataTables_Table_0_previous"><a
				href="myorder?page=${pageMaker.startPage-1}"
				aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">Previous</a></li>
		</c:if>
		<c:forEach begin="${pageMaker.startPage }" end="${pageMaker.endPage }"
			var="idx">
			<li <c:out value="${pageMaker.cri.page == idx?' class=active':''}"/>>
				<a href="myorder?page=${idx }" aria-controls="DataTables_Table_0"
				data-dt-idx="1" tabindex="0">${idx }</a>
			</li>
		</c:forEach>

		<c:if test="${pageMaker.next && pageMaker.endPage>0 }">
			<li class="paginate_button next" id="DataTables_Table_0_next"><a
				href="myorder?page=${pageMaker.endPage +1 }"
				aria-controls="DataTables_Table_0" data-dt-idx="7" tabindex="0">Next</a></li>
		</c:if>
	</ul>
</div>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script>
 var list = new Array(); // check된 리스트를 넘겨줄 list
	$(document).ready(function(){
		$("#deleteBtn").on("click",function(event){
			event.preventDefault();
			
			$("input[name='check']:checked").each(function ()
					{
						list.push($(this).val()); // value는 ono이므로 ono를 list에 넣는다.
					});
			console.log(list);
			jQuery.ajaxSettings.traditional = true;
			 
			$.ajax({
			    method : 'POST',
			    url   : '/mypage/delete',
			    data  : {
			        'list' : list
			    },
			    error: function(request, status, error) {
			        alert("code: "+request.status+"\n"+"message: "+request.responseText+"\n"+"error: "+error);
			    },
			    success : function(msg) {
			        alert(msg);   
			        self.location ="/mypage/myorder?page="+${cri.page };
			    }
			 
			});
		});
		
	});
</script>

<%@include file="../include/footer.jsp"%>