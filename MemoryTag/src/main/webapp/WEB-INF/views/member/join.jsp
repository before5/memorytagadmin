<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>
<!-- Main Content -->
<!-- Main Content -->
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">회원가입 </span>
			<div class="description"></div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-body">
						<form class="form-inline">
							<div class="form-group">
								<label for="exampleInputName2">ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</label> <input type="text" class="form-control" id="id"
									placeholder="ID를 입력해주세요">
							</div>

							<button type="button" class="btn btn-default" id="idcBtn">id
								check</button>
						</form>
					</div>
					<div class="card-body">
						<form class="form-inline">
							<div class="form-group">
								<label for="exampleInputName2">PW&nbsp;&nbsp;&nbsp;&nbsp;
								</label> <input type="text" class="form-control" id="pw"
									placeholder="PW를 입력해주세요..">
							</div>

						</form>
					</div>
					<div class="card-body">
						<form class="form-inline">
							<div class="form-group">
								<label for="exampleInputName2">Name&nbsp; </label> <input
									type="text" class="form-control" id="name"
									placeholder="Jane Doe">
							</div>


						</form>
					</div>
					<div class="card-body">
						<form class="form-inline">
							<div class="form-group">
								<label for="exampleInputEmail2">Email</label> <input type="email" class="form-control" id="email" placeholder="jane.doe@example.com">
							</div>
							
						</form>
					</div>
					<div class="card-body">
						<form class="form-inline">
							<div class="form-group">
								<label for="exampleInputName2">phone</label> <input  class="form-control" id="phone" >
							</div>
							
						</form>
					</div>
					<div class="card-body">
						<form class="form-inline">
							<div class="form-group">
								<label for="exampleInputEmail2">Gender&nbsp;</label> <select
									name="month" id="gender" aria-controls="DataTables_Table_0"
									class="form-control input-sm">
									<option value="M">남</option>
									<option value="W">여</option>

								</select>
							</div>

						</form>
					</div>
					<div class="card-body">
						<form class="form-inline">
							<div class="form-group">
								<label for="exampleInputName2">birth&nbsp; </label> <select
									name="year" id="year" aria-controls="DataTables_Table_0"
									class="form-control input-sm">
									<option value="1980">1980</option>
									<option value="1981">1981</option>
									<option value="1982">1982</option>
									<option value="1983">1983</option>
									<option value="1984">1984</option>
									<option value="1985">1985</option>
									<option value="1986">1986</option>
									<option value="1987">1987</option>
									<option value="1988">1988</option>
									<option value="1989">1989</option>
									<option value="1990">1990</option>
									<option value="1991">1991</option>
									<option value="1992">1992</option>
									<option value="1993">1993</option>
									<option value="1994">1994</option>
									<option value="1995">1995</option>
									<option value="1996">1996</option>
									<option value="1997">1997</option>
									<option value="1998">1998</option>
									<option value="1999">1999</option>
									<option value="2000">2000</option>
								</select>년 <select name="year" id="month"
									aria-controls="DataTables_Table_0"
									class="form-control input-sm">
									<option value="01">1</option>
									<option value="02">2</option>
									<option value="03">3</option>
									<option value="04">4</option>
									<option value="05">5</option>
									<option value="06">6</option>
									<option value="07">7</option>
									<option value="08">8</option>
									<option value="09">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
								</select>월 <select name="year" id="day"
									aria-controls="DataTables_Table_0"
									class="form-control input-sm">
									<option value="01">1</option>
									<option value="02">2</option>
									<option value="03">3</option>
									<option value="04">4</option>
									<option value="05">5</option>
									<option value="06">6</option>
									<option value="07">7</option>
									<option value="08">8</option>
									<option value="09">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
									<option value="31">31</option>
								</select>일
							</div>


						</form>
						
					</div>
					<div class="card-body">
						<label for="exampleInputName2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label> 
							<button type="button" class="btn btn-warning" id="regBtn">회원가입</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.1/handlebars.js"></script>
<script>
$(document).ready(function(){
	
	$("#idcBtn").on("click",function(event){
		console.log("id 체크버튼");
		event.preventDefault();
		var id = $("#id").val();
		
		$.ajax({
			type : 'get',
			url : '/member/check/'+id,
			headers : {"Content-Type" : "application/json"},
			dataType : 'text',
			success : function(result) {
				console.log(result);
				var status = result;
				if(status=='find'){
					alert("이미 사용중인 id입니다.");
					$("#id").val(" ");
				}else{
					alert("사용 가능합니다.");
				}
				}
			});
	});// 체크버튼
	$("#regBtn").on("click",function(event){
		console.log("등록버튼");
		var year = $("#year option:selected").val().toString();
		var month = $("#month option:selected").val().toString();
		var day = $("#day option:selected").val().toString();
		var birth = year+month+day;
					
		var obj = {id: $("#id").val() , pw: $("#pw").val() , name: $("#name").val(), email: $("#email").val(), gender:$("#gender option:selected").val().toString(), birth: birth, phone:$("#phone").val()};
		$.ajax({
			type : 'post',
			url : '/member/join',
			headers : {"Content-Type" : "application/json"},
			dataType : 'text',
			data : JSON.stringify(obj),
			success : function(result) {
				console.log(result);// reloadPage();
				alert("회원가입이 완료되었습니다.");
				self.location="/user/login";
				}
			});
	});// 등록 버튼
	
	
}); // document 

</script>

<%@include file="../include/footer.jsp"%>