<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- Main Content -->
<!--  삭제 처리를 위한 form 태그 작성 -->

<form role="form" method="post">

	<!-- <input type='hidden' name='ono' value="${OrderVO.ono }"> -->
	
</form>

 
<script type="text/javascript"
		src="/resources/bootstrap/lib/js/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		
			var formObj = $("form[role='form']");
			console.log("zzzz");
			console.log(formObj);
			
				//검색 버튼 클릭 이벤트
				$('#searchBtn').on("click", function(event) {
							console.log("dddddddddd")
							self.location = "list"
									+ '${pageMaker.makeQuery(1)}'
									+ "&searchType="
									+ $("select option:selected").val()
									+ "&keyword=" + $('#keywordInput').val();
						});
			
	
				//목록가기 버튼 클릭 이벤트
				$(".btn-warning").on("click", function() {
							console.log("read");
							self.location = "/order/list";
						});
				
				//checkbox 삭제 버튼 이벤트
				$(".btn-danger").on("click", function() {  //버튼 클릭시
					
					var checked = [];  // 변수 checked 배열 생성
					formObj.attr("action", "/order/removePage");  //order/removePage로 
					formObj.attr("method", "post");  //post로 전송
					
					//checkbox는 checked라는게 있음.
					//each는 배열의 각각이 실행됨
					$("input[name='check']:checked").each(function ()
					{
						// <input> 태그를 추가함
						$('<input />').attr('type', 'hidden')
	            		.attr('name', "ono") // <input> 태그 name=ono를 줌 
	            		.attr('value', $(this).val())  // <input> 태그 value=$(this).val()를 줌
	            		.appendTo(formObj);  //<input> 태그를 formObj에 추가함
						//인풋태그에 속성을 추가 시키고 인풋 태그를 폼에 추가 시킨다.
					});
					console.log(formObj.attr("ono")); //console.log로 출력해봄
					console.log("CheckBox 삭제");
					//서브밋을 날린다. 
					formObj.submit();  //formObj를 submit 전송함
					
				});
				
	});
</script>

<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">예약페이지  
			
	
			</span>
			<div class="description"></div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">

						<div class="card-title">
							<div class="title">예약 현황</div> 
						
						
						<div class='box-body'>

								<select name="searchType">
								
								    <!--  검색 조건이 없음 -->
									<option value="n"
										<c:out value="${cri.searchType == null?'selected':''}" />> <!-- 삼항연산자 -->
									</option>
									
									 <!--  예약일(t)로 검색 -->
									<option value="t" 
										<c:out value="${cri.searchType eq 't' ? 'selected':''}"/>> 
										예약일 </option>
									
									    <!-- 예약번호(c)로 검색 -->
										<option value="c"
											<c:out value="${cri.searchType eq 'c' ? 'selected':''}"/>>
									     	예약번호 </option>
										
										<!--  아이디	(w)로 검색 -->
										<option value="w"
											<c:out value="${cri.searchType eq 'w' ? 'selected':''}"/>>
											아이디 </option>
											
										<%-- <!--  예약일 또는 예약번호로 검색(tc) -->	
										<option value="tc"
											<c:out value="${cri.searchType eq 'tc' ? 'selected' : ''}"/>>
											예약일 OR 예약번호 </option>
										
										<!--  예약번호 또는 아이디로 검색(cw) -->
										<option value="cw"
											<c:out value="${cri.searchType eq 'cw' ? 'selected' :'' }"/>>
											예약번호 OR 아이디 </option>
											
										<!--  예약일 또는 예약번호 또는 아이디로 검색 -->
										<option value="tcw"
										 	<c:out value="${cri.searchType eq 'tcw' ? 'selected':'' }"/>>
											예약일 OR 예약번호 OR 아이디 --%>
								</select>
								
								<!-- 검색 입력 버튼 -->
								<input type="text" name='keyword' id="keywordInput"
									value='${cri.keyword }'>
								<button id='searchBtn'> 검색 </button>
								
							</div>
					</div>

					</div>
					<div class="card-body">
						<table class="table table-ordered">
							<tr>
								<th>check</th>
								<th>ONO</th>
								<th>ID</th>
								<th>DNO</th>
								<th>RegDate</th>
								<th>OrderDate</th>
							</tr>
															
							<c:forEach items="${list }" var="orderVO">
								 <tr>
								    <td><input type="checkbox" name="check" class="del-check" value="${orderVO.ono }" ></td>
									<td>${orderVO.ono }</td>
									<td>${orderVO.id }</td>
									<td>${orderVO.dno }</td>
									<td>${orderVO.regDate }</td>
									<td>${orderVO.orderDate }</td>
								</tr>
							</c:forEach>
						</table>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-warning"> 목록화면 </button>
						<button type="submit" class="btn btn-danger"> 삭제 </button>
					</div>
					<div class="text-center">
						<ul class="pagination">
		<c:if test="${ pageMaker.prev}">
			<li><a
				href="list${pageMaker.makeSearchURI(pageMaker.startPage -1) }">&laquo;</a></li>
		</c:if>

		<c:forEach begin="${pageMaker.startPage }" end="${pageMaker.endPage }"
			var="idx">
			<li
				<c:out value="${pageMaker.cri.page == idx?'class = active':'' }"/>>
				<a href="list${pageMaker.makeSearchURI(idx) }">${idx}</a>
		</c:forEach>

		<c:if test="${pageMaker.next && pageMaker.endPage > 0 }">
			<li><a
				href="list${pageMaker.makeSearchURI(pageMaker.endPage +1) }">&raquo;</a></li>
		</c:if>
	</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>



		
<%@include file="../include/footer.jsp"%>