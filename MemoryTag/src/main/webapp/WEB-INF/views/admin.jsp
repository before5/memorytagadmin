<%@ include file="./include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Main Content -->
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">Table</span>
			<div class="description">A bootstrap table for display list of
				data.</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">

						<div class="card-title">
							<div class="title">Managememt</div>
						</div>
					</div>
					<div class="card-body">
						<table class="table">
							<thead>
								<tr>
									<th></th>
									<th>ID</th>
									<th>PWD</th>
									<th>NAME</th>
									<th>GENDER</th>
									<th>GRADE</th>
									<th>BIRTH</th>
									<th>REGDATE</th>
									<th>UPDATEDATE</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${list }" var="user" varStatus="status">
									<tr>
										<td scope="row" width="30px">
											<div
												class="checkbox3 checkbox-success checkbox-inline checkbox-check checkbox-round  checkbox-light">
												<input type="checkbox"
													id="checkbox-fa-light-${status.count }"> <label
													for="checkbox-fa-light-${status.count }"></label>
											</div>
										</td>
										
										<td>${user.id }</td>
										<td>${user.pw }</td>
										<td>${user.name }</td>
										<td>${user.gender }</td>
										<td>
										 <select  name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="input-sm">
										 <option value="1" ${user.grade==1?'selected':'' }>1</option>
										 <option value="2" ${user.grade==2?'selected':'' }>2</option>
										 <option value="3" ${user.grade==3?'selected':'' }>3</option>
										 </select>
										

										</td>
										<td>${user.birth }</td>
										<td>${user.regDate }</td>
										<td>${user.updateDate }</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>



					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<%@include file="./include/footer.jsp"%>