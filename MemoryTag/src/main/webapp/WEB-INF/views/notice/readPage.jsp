
<%@ include file="../include/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
<!-- Main Content -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">Notice</span>
			<div class="description"></div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="card">

					<form role="form" method="post">
						<input type="hidden" name="nno" value="${notice.nno}"> <input
							type="hidden" name="page" value="${cri.page}"> <input
							type="hidden" name="perPageNum" value="${cri.perPageNum}">
						<input type="hidden" name="searchType" value="${cri.searchType}">
						<input type="hidden" name="keyword" value="${cri.keyword}">
					</form>

					<div class="card-body">
						<div class="form-group">
							<label for="exampleInputNno">Number</label> <input type="text"
								class="form-control" id="exampleInputNno" name="nno"
								value="${notice.nno}" readonly="readonly">
						</div>
						<div class="form-group">
							<label for="exampleInputTitle">Title</label> <input type="text"
								class="form-control" id="exampleInputtitle"
								value="${notice.title}" readonly="readonly">
						</div>
						<div class="form-group">
							<label for="exampleInputID">ID</label> <input type="text"
								class="form-control" id="exampleInputID" value="${notice.id}"
								readonly="readonly">
						</div>
						<div>
							<label for="exampleInputContent">Content</label>
							<textarea class="form-control" rows="13" readonly="readonly">${notice.content}</textarea>
						</div>


						<div class="form-group">
							<form enctype="multipart/form-data" id="my-awesome-dropzone">
								<label for="exampleInputFile">Attached File</label>
								<div class="dropzone" id="myDropzone">
									<c:forEach items="${attach }" var="file">
										<label>
											<div>
											
												<c:choose>
													<c:when test="${file.extension=='image/jpeg' }">
														<img src='/notice/displayFile?fileName=${file.stored_file_name }' />
													</c:when>
													<c:otherwise>
														<img src="/resources/bootstrap/img/file.png" />
													</c:otherwise>
												</c:choose>

											</div><a href='/notice/displayFile?fileName=${file.stored_file_name }'> ${file.original_file_name } </a>
										</label>
									</c:forEach>

								</div>
								<p class="help-block"></p>
							</form>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-warning">Modify</button>
						<button type="submit" class="btn btn-danger">Delete</button>
						<!--<button type="submit" class="btn btn-default">List All</button>-->
						<button type="submit" class="btn btn-primary">List Page</button>
					</div>
					<script>
						$(document).ready(function() {

							var formObj = $("form[role='form']");

							console.log(formObj);

							$(".btn-warning").on("click", function() {
								formObj.attr("action", "/notice/modifyPage");
								formObj.attr("method", "get");
								formObj.submit();
							});

							$(".btn-danger").on("click", function() {

								//formObj.attr("nno", "32");
								formObj.attr("action", "/notice/deletePage");
								formObj.submit();

							});

							//$(".btn-default")                자바스크립트의 주석 //
							//		.on(						html에서의 주석 <!-- --> 주석 처리 제대로 하기
							//				"click",
							//				function() {
							//					self.location = "/notice/listAll";
							//				});

							$(".btn-primary").on("click", function() {
								formObj.attr("action", "/notice/listPage");
								formObj.attr("method", "get");
								formObj.submit();
							});

							function checkImageType(fileName) {
								var pattern = /jpg|gif|png|jpeg/i;
								return fileName.match(pattern);
							}

						});
					</script>


				</div>
			</div>
		</div>


	</div>
</div>

<%@ include file="../include/footer.jsp"%>
