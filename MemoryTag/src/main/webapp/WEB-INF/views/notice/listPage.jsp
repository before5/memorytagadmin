
<%@ include file="../include/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<!-- Main Content -->
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">Notice</span>
			<div class="description">
			<select name="searchType">
		<option value="n"
		<c:out value="${cri.searchType == null?'selected':''}"/>>
		Search</option>
		<option value="t"
		<c:out value="${cri.searchType eq 't'?'selected':''}"/>>
		Title</option>
		<option value="c"
		<c:out value="${cri.searchType eq 'c'?'selected':''}"/>>
		Content</option>
		<option value="i"
		<c:out value="${cri.searchType eq 'i'?'selected':''}"/>>
		ID</option>
		<option value="tc"
		<c:out value="${cri.searchType eq 'tc'?'selected':''}"/>>
		Title OR Content</option>
		<option value="ci"
		<c:out value="${cri.searchType eq 'ci'?'selected':''}"/>>
		Content OR ID</option>
		<option value="tci"
		<c:out value="${cri.searchType eq 'tci'?'selected':''}"/>>
		Title OR Content OR ID</option>
		
		</select>
		<input type="text" name='keyword' id="keywordInput" value="${cri.keyword}">
		<button class="btn btn-info">Search</button>
		</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header"></div>
					<div class="card-body">
						<table class="table">
							<thead>
								<tr>
									<th>Number</th>
									<th>Title</th>
									<th>ID</th>
									<th>RegisterDate</th>
									<th>UpdateDate</th>
									<th>ViewCount</th>

								</tr>
							</thead>
							<tbody>
								<c:forEach items="${list }" var="noticeVO">
									<tr>



										<td scope="row">${noticeVO.nno }</td>
										<td><a
											href='/notice/readPage${pagemaker.makeURI(pagemaker.cri.page)}&nno=${noticeVO.nno }'>${noticeVO.title }</a></td>
										
										<td><a href=''>${noticeVO.id }</a></td>
										<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm"
												value="${noticeVO.regdate }" /></td>

										<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm"
												value="${noticeVO.updatedate}" /></td>
										<td>${noticeVO.viewcnt }</td>

									</tr>
								</c:forEach>
							</tbody>
						</table>

                         <div class="box-footer">
                      
						<button class="btn btn-default"><a href="register">Register</a></button>
				
					</div>

					</div>
					
					
					<div class="card-body">
						<div>
							<nav>
								<ul class="pagination">
									<c:if test="${pagemaker.prev}">
										<li><a
											href="listPage${pagemaker.makeURI(pagemaker.startPage-1)} "
											aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
										</a></li>
									</c:if>

									<c:forEach begin="${pagemaker.startPage}"
										end="${pagemaker.endPage}" var="idx">
										<li><c:out
												value="${pagemaker.cri.page == idx?'':'' } "></c:out>
											<a href="listPage?page=${idx}">${idx}</a></li>
									</c:forEach>

									<c:if test="${pagemaker.next && pagemaker.endPage>0}">
										<li><a
											href="listPage${pagemaker.makeURI(pagemaker.endPage+1)} "
											aria-label="Next"> <span aria-hidden="true">&raquo;</span>
										</a></li>
									</c:if>
								</ul>
							</nav>
						</div>




					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<script>
$(document).ready(function(){
	$(".btn-info").on("click",function(event){
		event.preventDefault();
		self.location = "listPage"
		+ "${pagemaker.makeURI(1)}"  // 검색할 때에는 makeURI를 쓴다.
		+ "&searchType="
		+ $("select option:selected").val() // 선택한 조건을 갖고온다.
		+ "&keyword=" + $('#keywordInput').val(); // 입력한 키워드를 갖고온다.
		

	});
});
</script>

<%@ include file="../include/footer.jsp"%>