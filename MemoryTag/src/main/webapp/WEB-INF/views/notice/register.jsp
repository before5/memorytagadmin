
  <%@ include file="../include/header.jsp"%> 
  <link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
  <script src="/resources/bootstrap/js/dropzone.min.js"></script>    
<!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <div class="page-title">
                        <span class="title">Notice</span>
                        <div class="description"></div>
                    </div>
        
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                               
                                <div class="card-body">
                                    <div class="form-group">
								<label for="exampleInputNno">Number</label> <input type="text"
									class="form-control" id="exampleInputNno" name="nno"
									value="${notice.nno}" readonly="readonly">
							</div>
                                        <div class="form-group">
                                            <label for="exampleInputTitle">Title</label>
                                            <input type="text" class="form-control" id="exampleInputtitle" placeholder="Enter title" name="title">
                                        </div>
                                           <div class="form-group">
                                            <label for="exampleInputID">ID</label>
                                            <input type="text" class="form-control" id="exampleInputID" placeholder="Enter id" name="id">
                                        </div>
                                        <div>
                                        <label for="exampleInputContent">Content</label>
                                        <textarea class="form-control" id="exampleInputcontent" rows="13" placeholder="Enter content"  name="content"></textarea>
                                    </div>
                                        
                                        <div class="form-group">
                                        
                                         <form  enctype="multipart/form-data" id="my-awesome-dropzone">
                                            <label for="exampleInputFile">File input</label>
                                           <div class="dropzone" id="myDropzone"></div>
                                            <p class="help-block"></p>
                                            </form>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                  
                 
                </div>
            </div>
            <script>
		$(document).ready(function(){
			Dropzone.formData = new FormData();
			//커스텀 dropzone생성
			Dropzone.options.myDropzone = {
					url : "/notice/register",
					autoProcessQueue : false,
					uploadMultiple : true,
					parallelUploads : 10,
					maxFiles : 10,//최대 파일 업로드 갯수
					addRemoveLinks : true,
					init : function(){
						dzClosure = this; // Makes sure that 'this' is understood inside the functions below.
						// for Dropzone to process the queue (instead of default form behavior):
						
						this.on("addedfile", function(file) {//파일이 추가되면 이벤트가 발생
							console.log(file.name);
						});
						 
						$(".btn-default").on("click", function(e) {
							// Make sure that the form isn't actually being sent.
							e.preventDefault();
							e.stopPropagation();
							dzClosure.processQueue();
							
						});
						this.on("sendingmultiple",
								function(data, xhr, formData) {
									formData.append("title", $(
											"#exampleInputtitle").val());
									formData.append("id", $(
											"#exampleInputID").val());
									formData.append("content", $(
											"#exampleInputcontent").val());
									
								});
						this.on("successmultiple", function(){
							location.href = '/notice/listPage';
						});
					}
						}
					
		});
</script>
   <%@ include file="../include/footer.jsp"%>         