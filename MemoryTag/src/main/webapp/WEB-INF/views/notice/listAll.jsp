
<%@ include file="../include/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Main Content -->
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">Notice</span>
			<div class="description"></div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header"></div>
					<div class="card-body">
						<table class="table">
							<thead>
								<tr>
									<th>Number</th>
									<th>Title</th>
									<th>ID</th>
									<th>RegisterDate</th>
									<th>UpdateDate</th>
									<th>ViewCount</th>

								</tr>
							</thead>
							<tbody>
								<c:forEach items="${list }" var="noticeVO">
									<tr>



										<td scope="row">${noticeVO.nno }</td>
										<td><a
											href='/notice/readPage${pagemaker.makeURI(pagemaker.cri.page)}&nno=${noticeVO.nno }'>${noticeVO.title }</a></td>
										<td><a href=''>${noticeVO.id }</a></td>
										<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm"
												value="${noticeVO.regdate }" /></td>

										<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm"
												value="${noticeVO.updatedate}" /></td>
										<td>${noticeVO.viewcnt }</td>

									</tr>
								</c:forEach>
							</tbody>
						</table>

						<div class="box-footer">

							<button class="btn btn-default">
								<a href="register">Register</a>
							</button>

						</div>

					</div>
				</div>
			</div>
		</div>

	</div>
</div>


<%@ include file="../include/footer.jsp"%>
