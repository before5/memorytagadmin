
<%@ include file="../include/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- Main Content -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">Notice</span>
			<div class="description"></div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="card">

					<form role="form" method="post">
						<input type="hidden" name="nno" value="${notice.nno}">
					</form>

					<div class="card-body">

						<div class="form-group">
							<label for="exampleInputTitle">Title</label> <input type="text"
								class="form-control" id="exampleInputtitle"
								value="${notice.title}" readonly="readonly">
						</div>
						<div class="form-group">
							<label for="exampleInputID">ID</label> <input type="text"
								class="form-control" id="exampleInputID" value="${notice.id}" readonly="readonly">
						</div>
						<div>
							<label for="exampleInputContent">Content</label>
							<textarea class="form-control" rows="13" readonly="readonly">${notice.content}</textarea>
						</div>


						<div class="form-group">
							<label for="exampleInputFile">Attached File </label> <input
								type="file" id="exampleInputFile">
							<p class="help-block"></p>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-warning">Modify</button>
							<button type="submit" class="btn btn-danger">Delete</button>
							<button type="submit" class="btn btn-default">List All</button>
						</div>
						<script>
							$(document).ready(function() {

								var formObj = $("form[role='form']");

								console.log(formObj);

								$(".btn-warning").on("click", function() {
									formObj.attr("action", "/notice/modify");
									formObj.attr("method", "get");
									formObj.submit();
								});

								$(".btn-danger").on("click", function() {
									console.log("클릭");
									//formObj.attr("nno", "32");
									formObj.attr("action", "/notice/delete");
									formObj.submit();
								});

								$(".btn-default").on("click", function() {
									self.location = "/notice/listAll";
								});

							});
						</script>

					</div>
				</div>
			</div>
		</div>


	</div>
</div>

<%@ include file="../include/footer.jsp"%>
