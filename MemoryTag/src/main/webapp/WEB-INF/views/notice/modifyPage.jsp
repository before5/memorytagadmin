
<%@ include file="../include/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- Main Content -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">Notice</span>
			<div class="description"></div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="card">

					<form role="form" action="modifyPage" method="post">

						<input type="hidden" name="nno" value="${notice.nno}"> 
						<input type="hidden" name="page" value="${cri.page}"> 
						<input type="hidden" name="perPageNum" value="${cri.perPageNum}">
						<input type="hidden" name="searchType" value="${cri.searchType}">
						<input type="hidden" name="keyword" value="${cri.keyword}">
						<div class="card-body">
							
							<div class="form-group">
								<label for="exampleInputNno">Number</label> <input type="text"
									class="form-control" id="exampleInputNno" name="nno"
									value="${notice.nno}" readonly="readonly">
							</div>
							
							<div class="form-group">
								<label for="exampleInputTitle">Title</label> <input type="text"
									class="form-control" id="exampleInputtitle" name="title"
									value="${notice.title}">
							</div>
							<div class="form-group">
								<label for="exampleInputID">ID</label> <input type="text"
									class="form-control" id="exampleInputID" value="${notice.id}"
									name="id" readonly="readonly">
							</div>
							<div>
								<label for="exampleInputContent">Content</label>
								<textarea class="form-control" rows="13" name="content">${notice.content}</textarea>
							</div>



							<div class="form-group">
								<label for="exampleInputFile">Attached File </label> <input
									type="file" id="exampleInputFile">
								<p class="help-block"></p>
							</div>
					</form>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-warning">Cancel</button>

					<button type="submit" class="btn btn-default">Save</button>
				</div>


				<script>
					$(document).ready(function() {

						var formObj = $("form[role='form']");

						console.log(formObj);

						$(".btn-warning").on("click", function() {
							self.location = "/notice/listPage?page=${cri.page}&perPageNum=${cri.perPageNum}"+"&sesarchType=${cri.searchType}&keyword=${cri.keyword}";
						});

						$(".btn-default").on("click", function() {
							formObj.submit();
						});

					});
				</script>
			</div>
		</div>
	</div>


</div>
</div>

<%@ include file="../include/footer.jsp"%>