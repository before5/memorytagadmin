<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../include/header.jsp"%>
<!-- Main Content -->
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">Q & A</span>
			<div class="description"></div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<form role="form" method="post">
						<input type="hidden" name="qno" value="${questionVO.qno}">
						<input type="hidden" name="page" value="${cri.page}">
					</form>

					<div class="card-body">
						<form>
							<div class="form-group">
								<label for="exampleInputTitle">Title</label> <input type="text"
									class="form-control" id="exampleInputtitle"
									value="${questionVO.title}" readonly="readonly">
							</div>
							<div class="form-group">
								<label for="exampleInputID">ID</label> <input type="text"
									class="form-control" id="exampleInputID"
									value="${questionVO.id}" readonly="readonly">
							</div>
							<div>
								<label for="exampleInputContent">Content</label>
								<textarea class="form-control" rows="13" readonly="readonly">${questionVO.content}</textarea>
							</div>

							<button type="submit" class="btn btn-danger" id="deleteBtn">Delete</button>
							<button type="submit" class="btn btn-default" id="goListBtn">
								List</button>
						</form>
						<div class="form-group">
							<label for="exampleInputName2">Replyer (ID)</label> <input
								type="text" class="form-control" id="replyer" name='replyID' 
								value='${id }' readonly="readonly">

						</div>
						<div class="form-group">

							<label for="exampleInputTitle">Add Reply</label> <input
								type="text" class="form-control" id="replyText"
								placeholder="댓글을 작성해주세요..">
						</div>
						<button type="button" class="btn btn-warning" id="addReplyBtn">ADD
							Reply</button>
						<button type="button" class="btn btn-warning" id="modifyReplyBtn">Modify
							Reply</button>
						<button type="button" class="btn btn-warning" id="deleteReplyBtn">Delete
							Reply</button>

						<div class="card card-success">
							<div class="card-header">
								<div class="card-title">
									<div class="title">
										<i class="fa fa-comments-o"></i>commant
									</div>
								</div>
								<div class="clear-both"></div>
							</div>
							<div class="card-body no-padding">
								<ul class="message-list">

								</ul>
							</div>
						</div>
						<ul class="pagination">

						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade modal-danger" id="modalDanger" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Modal Danger</h4>
					</div>
					<div class="modal-body">
						<p>
							<input type="text" id="replyMessage">
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-danger">OK</button>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.1/handlebars.js"></script>
<script>
function printPaging(pageMaker){
	
	console.log(pageMaker);
	 var str ="";

	 if(pageMaker.prev){
		 str +="<li class='paginate_button previous' id='DataTables_Table_0_previous'><a href='"+(pageMaker.startPage-1)+"'> <span aria-hidden='true'> << </a></li>";
		 
	 }

	 for(var i=pageMaker.startPage, len = pageMaker.endPage; i<=len; i++){

		 str += " <li class='paginate_button'><a href='"+i+"'>"+i+"</a></li>";
	 }
	 
	 if(pageMaker.next){
		
		 str +="<li class='paginate_button previous' id='DataTables_Table_0_previous'><a href='"+(pageMaker.endPage+1)+"'> <span aria-hidden='true'> << </a></li>";
	 }
	 
	 $('.pagination').html(str);
};
	$(document).ready(function() {
						var qno = ${questionVO.qno 	}; // qno를 선언했다.
						var id = $("#replyer").val(); // 사용자의 id

						console.log(id);
						$("#addReplyBtn").on("click",function(event) {
							event.preventDefault();
							console.log(qno);
							console.log($("#replyText").val());
							console.log($("#replyer").val());
							var obj = {qno : qno, replyText : $("#replyText").val(),replyer : $("#replyer").val()}
							// json타입으로 데이터를 보내주기 위한 준비 
							$.ajax({
								type : 'post',
								url : 'http://localhost:8081/replies/registReply',
								headers : {"Content-Type" : "application/json"},
								dataType : 'text',
								data : JSON.stringify(obj),
								success : function(result) {
									console.log(result);// reloadPage();
									getPageList(1);
									}
								});
							$("#replyText").val("");
						//	$("#replyer").val(""); // 댓글 등록 완료되면 댓글 input칸 비우기!!!
						});
						/* 댓글 add버튼을 누르면 댓글이 달려야 한다. */

						// 처음 들어왔을 때 댓글 첫번째 페이지가 보여야한다. page번호를 주면 댓글이 나오는 페이지 
						function getPageList(page) {
							$.getJSON("/replies/" + qno + "/" + page,function(data) {
								//console.log(data.replyList);
								//console.log(data.pageMaker);
								var pageMaker = data.pageMaker;
								var source = "<li calss='messageLi' >"
											+"<div>"
											+"<span class='username' data-rno='{{rno}}' data-replyText='{{replyText}}' data-replyer='{{replyer}}'>"+"작성자:"+"{{replyer}}</span> <span"
											+"</span>"
											//+" class='message-datetime'>12 min ago</span>"
											+"<div class='message' data-rno='{{rno}}' data-replyText='{{replyText}}' data-replyer='{{replyer}}'>{{replyText}}</div>"
											//+"<button type='button' class = 'btn btn-primary btn-info' data-toggle='modal' data-target='#modalDanger'>  Modal Info</button>"
											+"</div>"
											+"</li>";
											//"<button type='button' class = 'btn btn-primary btn-info' data-toggle='modal' data-target='#modalDanger'>  Modal Info</button>"
								var template = Handlebars.compile(source);
								var replies = $(".message-list");
								replies.html("");

								var str = "";

								$(data.replyList).each(function(index,value) {
									console.log(value);
									str += template(value);
									});
									replies.html(str);
									console.log(str);
									printPaging(pageMaker);
								})
						};
						getPageList(1);
						
						$(".pagination").on("click","li a",function(event){
							event.preventDefault();
							replyPage = $(this).attr("href");
							console.log(replyPage);
							getPageList(replyPage);
						});// 댓글 페이지를 클릭했을 때 알맞을 댓글이 갱신되는 페이지 
						
						var formObj= $("form[role='form']"); // 위에 hidden으로 되어있는 페이지와 question객체
						console.log(formObj);
						
						// 문의사항 삭제 메세지
						$("#deleteBtn").on("click",function(event){
							event.preventDefault();
							console.log(qno);
							formObj.attr("action","/question/delete");
							formObj.submit();
							
						});
						//list버튼을 눌렀을 때 
						$("#goListBtn").on("click",function(event){
							event.preventDefault();
							formObj.attr("method", "get");
							formObj.attr("action", "/question/list");
							formObj.submit();
						});
						
						$(".message-list").on("click",".messageLi",function(event){
							console.log("눌렷나");
							var message = $(this);
							$("#replyMessage").val(message.find('.message').text());
							console.log(message);
						});
						
						// 댓글 클릭했을 때 위에 올리는것 
						 var rno;
						$(".message-list").on("click",function(event){
							console.log("클릭");
							console.log($(event.target));
							// 만약 내 아이디랑 data-replyer의 아이디가 같다면 수정가능 
							var replyer =$(event.target).attr("data-replyer");
							if(replyer == id){
								$("#replyText").val($(event.target).attr("data-replyText"));
								
								//$("#replyer").val($(event.target).attr("data-replyText"));
								 $("input[name=replyID]").attr("disabled",true);
								rno = $(event.target).attr("data-rno");
							}else{
								//같지않으면 alert 창
								alert("댓글을 수정할 권한이 없습니다.");
							}
						});
						
						//댓글 수정 버튼 
						$("#modifyReplyBtn").on("click",function(event){
							event.preventDefault();
							
							 $("input[name=replyID]").attr("disabled",false);
							
							var obj = {qno : qno, replyText : $("#replyText").val(), rno:rno, replyer:$("#replyer").val()}
							// json타입으로 데이터를 보내주기 위한 준비 
							$.ajax({
								type : 'put',
								url : 'http://localhost:8081/replies/'+rno,
								headers : {"Content-Type" : "application/json"},
								dataType : 'text',
								data : JSON.stringify(obj),
								success : function(result) {
									console.log(result);// reloadPage();
									getPageList(1);
									}
								});
							 $("#replyText").val("");
							//	$("#replyer").val("");
						});
						
						//댓글 삭제 버튼
						$("#deleteReplyBtn").on("click",function(event){
							event.preventDefault();
							$.ajax({
								type : 'delete',
								url : 'http://localhost:8081/replies/'+rno,
								headers : {"Content-Type" : "application/json"},
								dataType : 'text',
								data : JSON.stringify(rno),
								success : function(result) {
									console.log(result);// reloadPage();
									getPageList(1);
									}
								});
							 $("#replyText").val("");
							//	$("#replyer").val("");
						});
						
					});
</script>
<%@ include file="../include/footer.jsp"%>
