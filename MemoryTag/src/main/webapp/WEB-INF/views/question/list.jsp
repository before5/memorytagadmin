<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">Q & A</span>
			<div class="description"></div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">
						<div class="col-sm-12">
							<div class="sub-title">
								<span class="description"></span>
							</div>
							<table class="table table-striped">
								<thead>
									<tr>
										<th>#qno</th>
										<th>title</th>
										<th>ID</th>
										<th>regdate</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${list}" var="questionVO">
										<tr>
											<td>${questionVO.qno}</td>
											<td><a
												href='read?page=${cri.page }&qno=${questionVO.qno }'>${questionVO.title}</a></td>
											<td>${questionVO.id}</td>
											<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm"
													value="${questionVO.regdate}" /></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- 댓글 페이지 번호 나오는 곳 -->
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 댓글 페이지 번호 나오는 곳 -->
<div class="text-center">
	<ul class="pagination">
		<c:if test="${pageMaker.prev }">
			<li class="paginate_button previous" id="DataTables_Table_0_previous"><a
				href="list?page=${pageMaker.startPage-1}"
				aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">Previous</a></li>
		</c:if>
		<c:forEach begin="${pageMaker.startPage }" end="${pageMaker.endPage }"
			var="idx">
			<li <c:out value="${pageMaker.cri.page == idx?' class=active':''}"/>>
				<a href="list?page=${idx }" aria-controls="DataTables_Table_0"
				data-dt-idx="1" tabindex="0">${idx }</a>
			</li>
		</c:forEach>

		<c:if test="${pageMaker.next && pageMaker.endPage>0 }">
			<li class="paginate_button next" id="DataTables_Table_0_next"><a
				href="list?page=${pageMaker.endPage +1 }"
				aria-controls="DataTables_Table_0" data-dt-idx="7" tabindex="0">Next</a></li>
		</c:if>
	</ul>
</div>
<%@include file="../include/footer.jsp"%>