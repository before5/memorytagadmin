<%@ include file="../include/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Main Content -->
<!-- 폼 만들기 데이터 전송위하여 -->
		<form role="form"  method="post">

<input type='hidden' name='page' value="${cri.page }">
<input type='hidden' name='perPageNum' value="${cri.perPageNum }">
<input type='hidden' name='searchType' value="${cri.searchType }">
<input type='hidden' name='keyword' value="${cri.keyword }">
</form>			
					
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">Table</span>
			<div class="description">A bootstrap table for display list of
				data.</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">

						<div class="card-title">
							<div class="title">Managememt</div>
							<div class='box-body'>

								<select name="searchType">
									<option value="n"
										<c:out value="${cri.searchType == null?'select':'' }"/>>
										---</option>
									<option value="i"
										<c:out value="${cri.searchType eq 'i'?'select':'' }"/>>
										ID</option>
									<option value="na"
										<c:out value="${cri.searchType eq 'na'?'select':'' }"/>>
										Name</option>
									<option value="g"
										<c:out value="${cri.searchType eq 'g'?'select':'' }"/>>
										Grade</option>
									<option value="ina"
										<c:out value="${cri.searchType eq 'ina'?'select':'' }"/>>
										ID or Name</option>
								</select> 
								<input type="text" name='keyword' id="keywordInput"
									value='${cri.keyword}'>
								<button id='searchBtn'>검색</button>
							
							</div>
						</div>
					</div>
					<div class="card-body">
						<table class="table">
							<thead>
								<tr>
									<th></th>
									<th>ID</th>
									<th>PWD</th>
									<th>NAME</th>
									<th>GENDER</th>
									<th>GRADE</th>
									<th>BIRTH</th>
									<th>REGDATE</th>
									<th>UPDATEDATE</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${list }" var="user" varStatus="status">
									<tr>
										<td scope="row" width="30px">

										
										<div
												class="checkbox3 checkbox-success checkbox-inline checkbox-check checkbox-round  checkbox-light">
												<input type="checkbox" name="check" value="${user.id }"
													id="checkbox-fa-light-${status.count }"> <label
													for="checkbox-fa-light-${status.count }"></label>
											</div>
											

										</td>

										<td>${user.id }</td>
										<td>${user.pw }</td>
										<td>${user.name }</td>
										<td>${user.gender }</td>
										<td><select name="gradeBox"
											aria-controls="DataTables_Table_0" class="input-sm gradeBox" data-id="${user.id}">
												<option data-id="${user.id}" value="1" ${user.grade==1?'selected':'' }>1</option>
												<option data-id="${user.id}" value="2" ${user.grade==2?'selected':'' }>2</option>
												<option data-id="${user.id}" value="3" ${user.grade==3?'selected':'' }>3</option>
										</select></td>
										<td>${user.birth }</td>
										<td>${user.regDate }</td>
										<td>${user.updateDate }</td>
									</tr>
							
									</c:forEach>
							</tbody>
						</table>
					<div class="box-footer">
					<button type="submit" class="btn btn-danger">삭 제</button>
					<button type="button" class="btn btn-warning">수 정</button>
					</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<div class="text-center">
	<ul class="pagination">
		<c:if test="${ pageMaker.prev}">
			<li><a
				href="list${pageMaker.makeSearchURI(pageMaker.startPage -1) }">&laquo;</a></li>
		</c:if>

		<c:forEach begin="${pageMaker.startPage }" end="${pageMaker.endPage }"
			var="idx">
			<li
				<c:out value="${pageMaker.cri.page == idx?'class = active':'' }"/>>
				<a href="list${pageMaker.makeSearchURI(idx) }">${idx}</a>
		</c:forEach>

		<c:if test="${pageMaker.next && pageMaker.endPage > 0 }">
			<li><a
				href="list${pageMaker.makeSearchURI(pageMaker.endPage +1) }">&raquo;</a></li>
		</c:if>
	</ul>
</div>
<!--조회버튼 작동-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
 
<script>

	
	
	$(document).ready(
			function() {
				var formObj = $("form[role='form']");
				var vo ={};
				$('#searchBtn').on(
						"click",
						function(event) {
							self.location = "list" 
									+ '${pageMaker.makeURI(1)}'
									+ "&searchType="
									+ $("select option:selected").val()
									+ "&keyword=" + $('#keywordInput').val();
						});
					

						
						//삭제
						$(".btn-danger").on("click", function() {  //버튼 클릭시
							
							
							console.log("클릭댐");
							
							
							console.log(formObj);
							formObj.attr("action", "/admin/remove");  //order/removePage로 
							formObj.attr("method", "post");  //post로 전송
							
							//checkbox는 checked(체크되어짐)이라는게 있음.
							$("input[name='check']:checked").each(function ()
							{
								// <input> 태그를 추가함
								$('<input />').attr('type', 'hidden')
			            		.attr('name', "id") // <input> 태그 name=id를 줌 
			            		.attr('value', $(this).val())  // <input> 태그 value=$(this).val()를 줌
			            		.appendTo(formObj);  //<input> 태그를 formObj에 추가함
								//인풋태그에 속성을 추가 시키고 인풋 태그를 폼에 추가 시킨다.
							});
							console.log(formObj.attr("id")); //console.log로 출력해봄
							console.log("CheckBox 삭제");
							//서브밋을 날린다. 
							formObj.submit();  //formObj를 submit 전송함
							
						});
						
						//등급이 바꼈을때(change기능)아이디와 등급을 배열에 저장 
						$('.gradeBox').change(function() {
							  
						var id =$(this).attr("data-id");	//id를 저장합니다.
						var grade = $(this).val();			//grade를 저장합니다.
						vo[id]=grade;						//vo에 키로 id와 값으로 grade를 담습니다.
						console.log(id);
							console.log(grade);
							console.log(id+": "+vo[id])
							//vo.push({id:id,grade:grade});
							  
					});
						//수정키를  누를때
						$(".btn-warning").on("click", function() {
							console.log(JSON.stringify(vo));							
							
							$.ajax({					//보내야 할 값이 두개이므로 json을 사용합니다.	
								type:'post',			//Post방식으로
								url:'/admin/edit',		///admin/edit에
								hearders:{
									"Content-Type":"application/json"},//json방식으로
								data:vo,				//vo를 	
								dataType:'text',		//text로 보냅니다.
								success:function(result){		//성공하면 function이 작동합니다.
									console.log("result: "+result);
									if(result == 'SUCCESS'){
										alert("수정 되었습니다."); 
									}
								}
								
								
								
								
							});
							
						});
							
							
							
							
							
						
						
			
});
		
	

</script> 	

<%@include file="../include/footer.jsp"%>