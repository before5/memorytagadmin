
<%@ include file="../include/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!-- Main Content -->
<div class="container-fluid">
	<div class="side-body">

		<div class="row">
			<div class="col-xs-12 bs-example-modal">
				<div class="card">
					<div class="card-header">
						<div class="card-title">
							<div class="title">Login Member</div>
						</div>
						<div class="pull-right card-action">
							<div class="btn-group" role="group">
								<button type="button" class="btn btn-link" data-toggle="modal"
									data-target="#modalModalExample">
									<i class="fa fa-code"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="modal active">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										
										<h3 class="modal-title"><font color = "FF0000">ID 또는 PW를 잘못 입력하셨습니다</font></h3>
									</div>
									<form class="form-horizontal" action="/user/loginPost" method="POST">
										<div class="modal-body">
										
										
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-2 control-label">USERID</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" name="id" placeholder="ID를 입력하세요"}
														id="inputEmail3" > 																						
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">PASSWORD</label>
												<div class="col-sm-10">
													<input type="password" class="form-control" name="pw"
														id="inputPassword3" placeholder="PW를 입력하세요">
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-2 col-sm-10">
													<div
														class="checkbox3 checkbox-round checkbox-check checkbox-light">
														<input type="checkbox" id="checkbox-10" name="useCookie">
														<label for="checkbox-10"> Remember me </label>


													</div>
												</div>
											</div>
										</div>
										<div class="modal-footer">

											<button class="btn btn-primary">Login</button>
											<button type="button" class="btn btn-warning" id="joinBtn">회원가입</button>
										</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modalModalExample" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Modal Example</h4>
					</div>
					<div class="modal-body no-padding">
						<div id="code-preview-modal" class="code-preview">&lt;div
							class=&quot;modal&quot;&gt; &lt;div
							class=&quot;modal-dialog&quot;&gt; &lt;div
							class=&quot;modal-content&quot;&gt; &lt;div
							class=&quot;modal-header&quot;&gt; &lt;button
							type=&quot;button&quot; class=&quot;close&quot;
							data-dismiss=&quot;modal&quot;
							aria-label=&quot;Close&quot;&gt;&lt;span
							aria-hidden=&quot;true&quot;&gt;&times;&lt;/span&gt;&lt;/button&gt;
							&lt;h4 class=&quot;modal-title&quot;&gt;Modal title&lt;/h4&gt;
							&lt;/div&gt; &lt;div class=&quot;modal-body&quot;&gt;
							&lt;p&gt;One fine body&hellip;&lt;/p&gt; &lt;/div&gt; &lt;div
							class=&quot;modal-footer&quot;&gt; &lt;button
							type=&quot;button&quot; class=&quot;btn btn-default&quot;
							data-dismiss=&quot;modal&quot;&gt;Close&lt;/button&gt; &lt;button
							type=&quot;button&quot; class=&quot;btn btn-primary&quot;&gt;Save
							changes&lt;/button&gt; &lt;/div&gt; &lt;/div&gt; &lt;/div&gt;
							&lt;/div&gt;</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
	$("#joinBtn").on("click",function(event){
		event.preventDefault();
		self.location="/member/join";
	});
});

</script>
<%@ include file="../include/footer.jsp"%>











