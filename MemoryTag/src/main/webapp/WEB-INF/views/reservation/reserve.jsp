<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>


<style>
#container {
	overflow: hidden;
	height: 600px;
	position: relative;
}

#mapWrapper {
	width: 100%;
	height: 600px;
	z-index: 1;
}

#rvWrapper {
	width: 50%;
	height: 600px;
	top: 0;
	right: 0;
	position: absolute;
	z-index: 0;
}

#container.view_roadview #mapWrapper {
	width: 50%;
}

#roadviewControl {
	position: absolute;
	top: 5px;
	left: 5px;
	width: 65px;
	height: 24px;
	padding: 2px;
	z-index: 1;
	background: #f7f7f7;
	border-radius: 4px;
	border: 1px solid #c8c8c8;
	box-shadow: 0px 1px #888;
	cursor: pointer;
}

#roadviewControl span {
	background:
		url(http://i1.daumcdn.net/localimg/localimages/07/mapapidoc/mapworker.png)
		no-repeat;
	padding-left: 23px;
	height: 24px;
	font-size: 12px;
	display: inline-block;
	line-height: 2;
	font-weight: bold;
}

#roadviewControl.active {
	background: #ccc;
	box-shadow: 0px 1px #5F616D;
	border: 1px solid #7F818A;
}

#roadviewControl.active span {
	background:
		url(http://i1.daumcdn.net/localimg/localimages/07/mapapidoc/mapworker_on.png)
		no-repeat;
	color: #4C4E57;
}

#close {
	position: absolute;
	padding: 4px;
	top: 5px;
	left: 5px;
	cursor: pointer;
	background: #fff;
	border-radius: 4px;
	border: 1px solid #c8c8c8;
	box-shadow: 0px 1px #888;
}

#close .img {
	display: block;
	background:
		url(http://i1.daumcdn.net/localimg/localimages/07/mapapidoc/rv_close.png)
		no-repeat;
	width: 14px;
	height: 14px;
}
</style>

<!-- Main Content -->
<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">reservation </span>
			<div class="description"></div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="exampleInputTitle">Title</label> <input type="text"
								class="form-control" id="title">
						</div>
						<div class="form-group">
							<label for="exampleInputID">ID</label> <input type="text"
								class="form-control" id="id" value="${reservationVO.id}"
								readonly="readonly">
						</div>
						<div>
							<label for="exampleInputContent">Content</label>
							<textarea class="form-control" rows="13" id="text"></textarea>
						</div>
					</div>
					<div class="card-body">
						<form class="form-inline">
							<div class="form-group">
								<label for="exampleInputName2">예약일</label> <label> <select
									name="year" id="year" aria-controls="DataTables_Table_0"
									class="form-control input-sm">
										<option value="2016">2016</option>
										<option value="2017">2017</option>
										<option value="2018">2018</option>
										<option value="2019">2019</option>
								</select> 년
								</label> <label> <select name="month" id="month"
									aria-controls="DataTables_Table_0"
									class="form-control input-sm">
										<option value="01">1</option>
										<option value="02">2</option>
										<option value="03">3</option>
										<option value="04">4</option>
										<option value="05">5</option>
										<option value="06">6</option>
										<option value="07">7</option>
										<option value="08">8</option>
										<option value="09">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
								</select>월
								</label> <label> <select name="day" id="day"
									aria-controls="DataTables_Table_0"
									class="form-control input-sm">
										<option value="01">1</option>
										<option value="02">2</option>
										<option value="03">3</option>
										<option value="04">4</option>
										<option value="05">5</option>
										<option value="06">6</option>
										<option value="07">7</option>
										<option value="08">8</option>
										<option value="09">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
										<option value="21">21</option>
										<option value="22">22</option>
										<option value="23">23</option>
										<option value="24">24</option>
										<option value="25">25</option>
										<option value="26">26</option>
										<option value="27">27</option>
										<option value="28">28</option>
										<option value="29">29</option>
										<option value="30">30</option>
										<option value="31">31</option>
								</select>일
								</label> <label> <select name="time" id="time"
									aria-controls="DataTables_Table_0"
									class="form-control input-sm">
										<option value="01">1</option>
										<option value="02">2</option>
										<option value="03">3</option>
										<option value="04">4</option>
										<option value="05">5</option>
										<option value="06">6</option>
										<option value="07">7</option>
										<option value="08">8</option>
										<option value="09">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
										<option value="21">21</option>
										<option value="22">22</option>
										<option value="23">23</option>
										<option value="24">24</option>
								</select>시
								</label>
							</div>


						</form>
					</div>
					<div class="card-body">
						<form class="form-inline">
							<div class="form-group">
								<label for="exampleInputName2">폰트색깔</label> <label> <select
									name="fontcolor" id="fontcolor"
									aria-controls="DataTables_Table_0"
									class="form-control input-sm">
										<option value="red">빨강</option>
										<option value="yellow">노랑</option>
										<option value="blue">파랑</option>
										<option value="white">하양</option>
								</select> 색
								</label> <label for="exampleInputName2">폰트 사이즈</label> <label> <select
									name="fontsize" id="fontsize"
									aria-controls="DataTables_Table_0"
									class="form-control input-sm">
										<option value=10>10</option>
										<option value=15>15</option>
										<option value=20>20</option>
										<option value=25>25</option>
								</select> 색
								</label>
							</div>
						</form>
					</div>

					<div class="card-body">
						<label for="exampleInputName2">비밀번호 설정</label> <input type="text"
							class="form-control" id="password" placeholder="비밀번호를 입력하세요...">
						<label for="exampleInputName2">디바이스 선택</label>

						<!--  다음 지도 map zone -->

						<!--  지도를 담을 영역 만들기 -->

						<div id="container">
							<div id="rvWrapper">
								<div id="roadview" style="width: 100%; height: 100%;"></div>
								<!-- 로드뷰를 표시할 div 입니다 -->
								<div id="close" title="로드뷰닫기" onclick="closeRoadview()">
									<span class="img"></span>
								</div>
							</div>
							<div id="mapWrapper">
								<div id="map" style="width: 100%; height: 100%"></div>
								<!-- 지도를 표시할 div 입니다 -->
								<div id="roadviewControl" onclick="setRoadviewRoad()">
									<span>로드뷰</span>
								</div>
							</div>
						</div>

						<!-- <div id="map" style="width: 1000px; height: 600px";></div> -->

						<label for="exampleInputName2">파일 등록</label>

						<form action="/reservation/reserve" enctype="multipart/form-data"
							method="POST" id="my-awesome-dropzone">
							<!-- dropzone -->

							<div class="dropzone" id="myDropzone"></div>
						</form>
						<button type="submit" class="btn btn-default" id="reservationBtn">
							예약</button>

					</div>


				</div>
			</div>
		</div>
	</div>
</div>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.1/handlebars.js"></script>
<script src="/resources/bootstrap/js/dropzone.min.js"></script>
<!--  실제 지도를 그리는 Javascript API 불러오기-->
<script type="text/javascript"
	src="//apis.daum.net/maps/maps3.js?apikey=5c03d06423534a33518270664ff2e54e"></script>

<script>
	$(document).ready(
					function() {

						Dropzone.formData = new FormData();

						var dnoarray = new Array(); // dnos는 array이므로 
						//커스텀 dropzone생성
						Dropzone.options.myDropzone = {

							url : '/reservation/reserve',
							acceptedFiles : "image/*, video/*",
							autoProcessQueue : false,
							uploadMultiple : true,
							parallelUploads : 10,
							maxFiles : 1,//최대 파일 업로드 갯수 // 예약페이지 에서는 파일을 최대 1개만 등록할 수 있다. 
							maxFilesize : 1000,//100mb
							addRemoveLinks : true,
							init : function() {
								dzClosure = this; // dropzone인듯 
								// 예약 버튼을 눌렀을때
								$("#reservationBtn").on("click",
										function(event) {
											event.preventDefault();
											event.stopPropagation();

											dzClosure.processQueue();
										});

								this.on("sendingmultiple",function(data, xhr, formData) {

													var orderdate;

													var year = $("#year option:selected").val().toString();
													var month = $("#month option:selected").val().toString();
													var day = $(
															"#day option:selected")
															.val().toString();
													var time = $(
															"#time option:selected")
															.val().toString();
													var fontsize = $(
															"#fontsize option:selected")
															.val();
													var fontcolor = $(
															"#fontcolor option:selected")
															.val();
													//var dnos = $("#dno option:selected").val();   
													//dnoarray[0] = dnos;   
													orderdate = year + month
															+ day + time; // orderdate는 string 형식으로 보내줘야 date로 변환하기 때문에

													formData.append("id", $("#id").val());
													formData.append("title", $("#title").val());
													formData.append("text", $("#text").val());
													formData.append("orderdate",orderdate);
													formData.append("dnos",dnoarray);
													formData.append("pw", $("#password").val());
													formData.append("fontsize",fontsize);
													formData.append("fontcolor",fontcolor);

												});

								this.on("successmultiple", function() {
									/* location.href = '/board'; */
									alert("예약 성공!");
									location.href = '/order/list';
								});
							}

						}

						// ----------------------------- 지도 api -----------------------------------------------
						// ------------------------------아래로 전부 지도쪽 ----------------------------------------

						/* var overlayOn = false, // 지도 위에 로드뷰 오버레이가 추가된 상태를 가지고 있을 변수
						container = document.getElementById('container'), // 지도와 로드뷰를 감싸고 있는 div 입니다
						mapWrapper = document.getElementById('mapWrapper'), // 지도를 감싸고 있는 div 입니다
						//mapContainer = document.getElementById('map'), // 지도를 표시할 div 입니다 
						rvContainer = document.getElementById('roadview'); //로드뷰를 표시할 div 입니다
						 */

						var mapContainer = document.getElementById('map'), // 지도를 표시할 div  
						mapOption = {
							center : new daum.maps.LatLng(37.497968, 127.027664), // 지도의 중심좌표
							level : 3
						// 지도의 확대 레벨
						};

						var map = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

						map.markers = new Array(); // 배열생성

						map.removeMarkers = function() { //메소드정의
							for (var i = 0; i < map.markers.length; i++) {
								map.markers[i].remove(); //메소드 호출
							}
						}

						map.removeMarkers(); //맵에서 마커들을 지운다.

						$.getJSON('/reservation/device', function(data) {
							console.log(data);

							for (var i = 0; i < data.length; i++) {
								var marker = new daum.maps.Marker({
									map : map, // 마커를 표시할 지도
									position : new daum.maps.LatLng(
											data[i].lat, data[i].lng), // 마커의 위도 경도
								// title: data[i].dno, // 마커의 타이틀, 마커에 마우스를 올리면 타이틀이 표시됩니다

								});
								// 마커가 지도 위에 표시되도록 설정합니다
								marker.setMap(map); //맵에다가 마커를 뿌려주는 부분
								marker.remove = function() { //정의 만하는거
									this.setMap(null);
								}

								marker.dno = data[i].dno; //dno를 넣어줌
								map.markers.push(marker); //배열에 집어넣는 부분
							}

						});
						var drawingFlag = false; // 원이 그려지고 있는 상태를 가지고 있을 변수입니다
						var centerPosition; // 원의 중심좌표 입니다 전역변수
						var drawingCircle; // 그려지고 있는 원을 표시할 원 객체입니다
						var drawingLine; // 그려지고 있는 원의 반지름을 표시할 선 객체입니다
						var drawingOverlay; // 그려지고 있는 원의 반경을 표시할 커스텀오버레이 입니다
						var drawingDot; // 그려지고 있는 원의 중심점을 표시할 커스텀오버레이 입니다

						var circles = []; // 클릭으로 그려진 원과 반경 정보를 표시하는 선과 커스텀오버레이를 가지고 있을 배열입니다

						var clickLine // 마우스로 클릭한 좌표로 그려질 선 객체입니다
						var distanceOverlay; // 선의 거리정보를 표시할 커스텀오버레이 입니다
						var dots = {}; // 선이 그려지고 있을때 클릭할 때마다 클릭 지점과 거리를 표시하는 커스텀 오버레이 배열입니다.

						// 지도에 클릭 이벤트를 등록합니다
						daum.maps.event
								.addListener(
										map,
										'click',
										function(mouseEvent) {

											// 클릭 이벤트가 발생했을 때 원을 그리고 있는 상태가 아니면 중심좌표를 클릭한 지점으로 설정합니다
											if (!drawingFlag) {

												// 상태를 그리고있는 상태로 변경합니다
												drawingFlag = true;

												// 원이 그려질 중심좌표를 클릭한 위치로 설정합니다
												centerPosition = mouseEvent.latLng;
												console.log(centerPosition)

												// 그려지고 있는 원의 반경을 표시할 선 객체를 생성합니다
												if (!drawingLine) {
													drawingLine = new daum.maps.Polyline(
															{
																strokeWeight : 3, // 선의 두께입니다
																strokeColor : '#00a0e9', // 선의 색깔입니다
																strokeOpacity : 1, // 선의 불투명도입니다 0에서 1 사이값이며 0에 가까울수록 투명합니다
																strokeStyle : 'solid' // 선의 스타일입니다
															});
												}

												// 그려지고 있는 원을 표시할 원 객체를 생성합니다
												if (!drawingCircle) {
													drawingCircle = new daum.maps.Circle(
															{
																strokeWeight : 1, // 선의 두께입니다
																strokeColor : '#00a0e9', // 선의 색깔입니다
																strokeOpacity : 0.1, // 선의 불투명도입니다 0에서 1 사이값이며 0에 가까울수록 투명합니다
																strokeStyle : 'solid', // 선의 스타일입니다
																fillColor : '#00a0e9', // 채우기 색깔입니다
																fillOpacity : 0.2
															// 채우기 불투명도입니다
															});
												}

												// 그려지고 있는 원의 반경 정보를 표시할 커스텀오버레이를 생성합니다
												if (!drawingOverlay) {
													drawingOverlay = new daum.maps.CustomOverlay(
															{
																xAnchor : 0,
																yAnchor : 0,
																zIndex : 1
															});
												}
											}
										});

						// 지도에 마우스무브 이벤트를 등록합니다
						// 원을 그리고있는 상태에서 마우스무브 이벤트가 발생하면 그려질 원의 위치와 반경정보를 동적으로 보여주도록 합니다
						daum.maps.event
								.addListener(
										map,
										'mousemove',
										function(mouseEvent) {

											// 마우스무브 이벤트가 발생했을 때 원을 그리고있는 상태이면
											if (!drawingFlag) {
											} else {

												// 마우스 커서의 현재 위치를 얻어옵니다
												var mousePosition = mouseEvent.latLng;

												// 그려지고 있는 선을 표시할 좌표 배열입니다. 클릭한 중심좌표와 마우스커서의 위치로 설정합니다
												var linePath = [
														centerPosition,
														mousePosition ];

												// 그려지고 있는 선을 표시할 선 객체에 좌표 배열을 설정합니다
												drawingLine.setPath(linePath);

												// 원의 반지름을 선 객체를 이용해서 얻어옵니다
												var length = drawingLine
														.getLength();

												if (length > 0) {

													// 그려지고 있는 원의 중심좌표와 반지름입니다
													var circleOptions = {
														center : centerPosition,
														radius : length
													};

													// 그려지고 있는 원의 옵션을 설정합니다
													drawingCircle
															.setOptions(circleOptions);

													// 반경 정보를 표시할 커스텀오버레이의 내용입니다
													var radius = Math
															.round(drawingCircle
																	.getRadius()), content = '<div class="info">반경 <span class="number">'
															+ radius
															+ '</span>m</div>';

													//var radiusMarker = Math.round(lengthMarker), content = '<div class="info">위치 <span class="loc">' + markerPosition + '</span>m</div>';

													// 반경 정보를 표시할 커스텀 오버레이의 좌표를 마우스커서 위치로 설정합니다
													drawingOverlay
															.setPosition(mousePosition);

													// 반경 정보를 표시할 커스텀 오버레이의 표시할 내용을 설정합니다
													drawingOverlay
															.setContent(content);

													// 그려지고 있는 원을 지도에 표시합니다
													drawingCircle.setMap(map);

													// 그려지고 있는 선을 지도에 표시합니다
													drawingLine.setMap(map);

													// 그려지고 있는 원의 반경정보 커스텀 오버레이를 지도에 표시합니다
													drawingOverlay.setMap(map);

												} else {

													drawingCircle.setMap(null);
													drawingLine.setMap(null);
													drawingOverlay.setMap(null);

												}
											}

										});

						// 지도에 마우스 오른쪽 클릭이벤트를 등록합니다
						// 원을 그리고있는 상태에서 마우스 오른쪽 클릭 이벤트가 발생하면
						// 마우스 오른쪽 클릭한 위치를 기준으로 원과 원의 반경정보를 표시하는 선과 커스텀 오버레이를 표시하고 그리기를 종료합니다
						daum.maps.event
								.addListener(
										map,
										'rightclick',
										function(mouseEvent) {

											if (!drawingFlag) {
											} else {

												// 마우스로 오른쪽 클릭한 위치입니다
												var rClickPosition = mouseEvent.latLng;

												// 원의 반경을 표시할 선 객체를 생성합니다
												var polyline = new daum.maps.Polyline(
														{
															path : [
																	centerPosition,
																	rClickPosition ], // 선을 구성하는 좌표 배열입니다. 원의 중심좌표와 클릭한 위치로 설정합니다
															strokeWeight : 3, // 선의 두께 입니다
															strokeColor : '#00a0e9', // 선의 색깔입니다
															strokeOpacity : 1, // 선의 불투명도입니다 0에서 1 사이값이며 0에 가까울수록 투명합니다
															strokeStyle : 'solid' // 선의 스타일입니다
														});

												// 원 객체를 생성합니다
												var circle = new daum.maps.Circle(
														{
															center : centerPosition, // 원의 중심좌표입니다
															radius : polyline
																	.getLength(), // 원의 반지름입니다 m 단위 이며 선 객체를 이용해서 얻어옵니다
															strokeWeight : 1, // 선의 두께입니다
															strokeColor : '#00a0e9', // 선의 색깔입니다
															strokeOpacity : 0.1, // 선의 불투명도입니다 0에서 1 사이값이며 0에 가까울수록 투명합니다
															strokeStyle : 'solid', // 선의 스타일입니다
															fillColor : '#00a0e9', // 채우기 색깔입니다
															fillOpacity : 0.2
														// 채우기 불투명도입니다
														});

												var radius = Math.round(circle
														.getRadius()), // 원의 반경 정보를 얻어옵니다
												content = getTimeHTML(radius); // 커스텀 오버레이에 표시할 반경 정보입니다

												//var radiusMarker = Math.round(lengthMarker), content = getTimeHTML(radiusMarker);

												// 반경정보를 표시할 커스텀 오버레이를 생성합니다
												var radiusOverlay = new daum.maps.CustomOverlay(
														{
															content : content, // 표시할 내용입니다
															position : rClickPosition, // 표시할 위치입니다. 클릭한 위치로 설정합니다
															xAnchor : 0,
															yAnchor : 0,
															zIndex : 1
														});

												// 원을 지도에 표시합니다
												circle.setMap(map);

												// 선을 지도에 표시합니다
												//polyline.setMap(map);

												// 반경 정보 커스텀 오버레이를 지도에 표시합니다
												radiusOverlay.setMap(map);

												// 배열에 담을 객체입니다. 원, 선, 커스텀오버레이 객체를 가지고 있습니다
												var radiusObj = {
													'polyline' : polyline,
													'circle' : circle,
													'overlay' : radiusOverlay
												};

												//시작위치와 끝위치를 계산을해요
												//그 거리를 구해요

												//그리고 시작위치와 마커들 간의 위치를 계산을 해요
												//그 거리를 구해요

												//두개의 거리의 차이를 구해요
												//그런다음에 마커의 위치가 더 적으면 true

												//true면 포함이 된 상태입니다
												//;그러면 출력

												for (i = 0; i < map.markers.length; i++) {
													if (containMaker(
															rClickPosition,
															map.markers[i]
																	.getPosition())) {
														console
																.log(map.markers[i]
																		.getPosition());
														console
																.log(map.markers[i].dno);
														dnoarray
																.push(map.markers[i].dno);
													}
												}

												// 배열에 추가합니다
												// 이 배열을 이용해서 "모두 지우기" 버튼을 클릭했을 때 지도에 그려진 원, 선, 커스텀오버레이들을 지웁니다
												circles.push(radiusObj);

												// 그리기 상태를 그리고 있지 않는 상태로 바꿉니다
												drawingFlag = false;

												// 중심 좌표를 초기화 합니다
												centerPosition = null;

												// 그려지고 있는 원, 선, 커스텀오버레이를 지도에서 제거합니다
												drawingCircle.setMap(null);
												drawingLine.setMap(null);
												drawingOverlay.setMap(null);
											}

										});

						function containMaker(rClickPosition, makerPosition) {
							return getDistance(centerPosition, makerPosition) < getDistance(
									centerPosition, rClickPosition);
						}

						function getDistance(p1, p2) {
							//직교좌표 거리 계산
							return Math.sqrt(Math.pow((p1.getLat() - p2
									.getLat()), 2)
									+ Math.pow((p1.getLng() - p2.getLng()), 2));
						}

						// 지도에 표시되어 있는 모든 원과 반경정보를 표시하는 선, 커스텀 오버레이를 지도에서 제거합니다
						function removeCircles() {
							for (var i = 0; i < circles.length; i++) {
								circles[i].circle.setMap(null);
								circles[i].polyline.setMap(null);
								circles[i].overlay.setMap(null);
							}
							circles = [];
						}

						// 마우스 우클릭 하여 원 그리기가 종료됐을 때 호출하여
						// 그려진 원의 반경 정보와 반경에 대한 도보, 자전거 시간을 계산하여
						// HTML Content를 만들어 리턴하는 함수입니다
						function getTimeHTML(distance) {

							// 거리와 도보 시간, 자전거 시간을 가지고 HTML Content를 만들어 리턴합니다
							var content = '<ul class="info">';
							content += '    <li>';
							content += '        <span class="label">총거리</span><span class="number">'
									+ distance + '</span>m';
							content += '    </li>';
							content += '</ul>'

							return content;
						}
					}); // document ready
</script>
<%@include file="../include/footer.jsp"%>